<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/signup', 'AuthController@signupPage')->name('sign_up');
Route::post('/signup', 'AuthController@handleSignUp')->name('handle_signup');
Route::get('/', 'AuthController@login')->name('get_login');
Route::post('/login', 'AuthController@handleLogin')->name('post_login');
Route::get('/logout', 'AuthController@logout')->name('logout');
Route::get('activation/{userId}/{code}', 'AuthController@activateUserView')->name('activate_user');

Route::get('dashboard', 'DashboardController@index')->name('user_dashboard');
Route::get('/confirmation/{id}', 'AuthController@confirmationPage')->name('confirmation-page');
Route::post('/confirmation', 'AuthController@confirmUser')->name('confirm-user');
Route::get('/resend/confirmation/{id}', 'AuthController@resendConfirmationMessage')->name('resend-confirmation-code');
// Route::get('tax-payer/register', 'TaxPayerController@registerTaxPayer')->name('tax-payers-manage');
// Route::post('tax-payer/store', 'TaxPayerController@storeTaxPayer')->name('taxpayer-store');
// Route::get('tax-payer/{id}/delete', 'TaxPayerController@deleteTaxPayer')->name('delete-taxpayer');
// Route::post('tax-payer/{id}/update', 'TaxPayerController@updateTaxPayer')->name('edit-taxpayer');

Route::group(['prefix' => 'users', 'middleware' =>['logged.in']], function() {
  Route::get('/manage', 'UserController@index')->name('manage_users');
  Route::post('/create', 'UserController@create')->name('store-user');
  Route::post('/update/{id}', 'UserController@update')->name('edit-user');
  Route::get('/{id}/delete', 'UserController@deleteDataEntryPersonel')->name('delete-user');
});

Route::group(['prefix' => 'taxpayers/group', 'middleware' =>['logged.in']], function() {
  Route::get('/manage', 'TaxPayerGroupController@index')->name('manage-group');
  Route::post('/create', 'TaxPayerGroupController@create')->name('store-group');
  Route::post('/update/{id}', 'TaxPayerGroupController@update')->name('edit-group');
  Route::get('/{id}/delete', 'TaxPayerGroupController@deleteTaxPayerGroup')->name('delete-group');
});

Route::group(['prefix' => 'reports', 'middleware' =>['logged.in']], function() {
  Route::get('/', 'ReportsController@getReports')->name('get-reports');
  Route::post('/', 'ReportsController@reports')->name('reports');
});

//mdas
Route::group(['prefix' => 'mda', 'middleware' =>['logged.in']], function() {
  Route::get('/', 'MdaController@index')->name('mdas');
  Route::post('/create', 'MdaController@store')->name('store_mda');
  Route::post('/update/{id}', 'MdaController@update')->name('edit_mda');
  Route::get('/delete/{id}', 'MdaController@delete')->name('delete_mda');
});

//Revenue headings routes
Route::group(['prefix' => 'heading', 'middleware' =>['logged.in']], function() {
  Route::get('/', 'HeadingController@index')->name('headings');
  Route::post('/create', 'HeadingController@store')->name('store_heading');
  Route::post('/update/{id}', 'HeadingController@update')->name('edit_heading');
  Route::get('/delete/{id}', 'HeadingController@delete')->name('delete_heading');
});

// Revenue Sub-heading routes
Route::group(['prefix' => 'sub-heading', 'middleware' =>['logged.in']], function() {
  Route::get('/', 'SubHeadingController@index')->name('sub_headings');
  Route::post('/create', 'SubHeadingController@store')->name('store_subheading');
  Route::post('/update/{id}', 'SubHeadingController@update')->name('edit_subheading');
  Route::get('/delete/{id}', 'SubHeadingController@delete')->name('delete_subheading');
});

//TaxPayer routes
Route::group(['prefix' => 'taxpayers', 'middleware' =>['logged.in']], function() {
  Route::get('/', 'UserController@allTaxpayers')->name('tax_payers');
  Route::post('/create', 'TaxPayerController@store')->name('store_taxpayer');
  Route::post('/update/{id}', 'TaxPayerController@update')->name('edit_taxpayer');
  Route::get('/delete/{id}', 'TaxPayerController@delete')->name('delete_taxpayer');
});

//Assessments routes
Route::group(['prefix' => 'invoices', 'middleware' => ['logged.in']], function() {
  Route::get('/', 'InvoiceController@index')->name('invoices');
  Route::get('/generate-invoice/{id}', 'InvoiceController@create')->name('create_invoice');
  Route::post('/generate-invoice', 'InvoiceController@store')->name('store_invoice');
  Route::get('/show-invoice/{id}', 'InvoiceController@show')->name('show_invoice');
  Route::get('/delete/{id}', 'InvoiceController@delete')->name('delete_invoice');
});

/**
 * Transactions
 */
Route::group(['prefix' => 'transactions', 'middleware' =>['logged.in']], function() {
  Route::get('/', 'TransactionController@index')->name('transactions');
  Route::post('/store', 'TransactionController@storeTransaction')->name('store_transactions');
});

//Ajax call
Route::get('/ajax/assessment', 'SubHeadingController@findSubHeadings')->name('find_subheading');
Route::get('/ajax/unit-price', 'SubHeadingController@getPrice')->name('subheading_price');

/**
 * Quicl payment route
 */
Route::get('/service-payment', 'AssessmentController@quickPayment')->name('quick_payment');

/**
 * Paystack Integration Route
 * This was created to handle payments for the PayStack payment gateway integration.
 */
Route::get('/pay/{id}', 'PaymentController@paymentPage')->name('payment_page');
Route::post('/pay', 'PaymentController@redirectToGateway')->name('paystack_pay');
Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');

/**
 * Flutterwave Rave Integration Route
 * This was created to handle payments for the Flutterwave Rave payment gateway integration.
 */
// Route::get('/payss', 'RaveController@paymentPage')->name('rave_page');
// Route::post('/payss', 'RaveController@initialize')->name('pay');
// Route::post('/rave/callback', 'RaveController@callback')->name('callback');


Route::group(['prefix' => 'assessments', 'middleware' => ['logged.in']], function() {
  Route::get('/', 'AssessmentController@index')->name('assessments');
  Route::get('/create', 'AssessmentController@createAssessment')->name('create-assessment');
  Route::post('/save', 'AssessmentController@saveAssessment')->name('store-assessment');
  Route::get('/view/{id}', 'AssessmentController@viewAssessment')->name('view-assessment');
});
