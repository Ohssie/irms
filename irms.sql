-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: irms
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Dumping data for table `activations`
--

LOCK TABLES `activations` WRITE;
/*!40000 ALTER TABLE `activations` DISABLE KEYS */;
INSERT INTO `activations` VALUES (1,1,'2PGSytltnWEIOlDcviDGyOUsPBc5apgp',1,'2019-04-27 20:41:19','2019-04-27 20:41:19','2019-04-27 20:41:19'),(2,2,'ba6Rltv8K9RQMhAdIT22qXjKUGwCfAWR',1,'2019-04-27 20:41:19','2019-04-27 20:41:19','2019-04-27 20:41:19'),(3,3,'gUD0dlYvmnbfhXPWim17vRzy0vCF38WY',1,'2019-04-27 20:41:19','2019-04-27 20:41:19','2019-04-27 20:41:19'),(4,4,'tpHaiwhbnHlnJbn4gN9L94tZFZAqPHga',1,'2019-04-27 20:41:20','2019-04-27 20:41:20','2019-04-27 20:41:20'),(5,5,'vaZwHj6D0c2n9rcba7dMmTpY5UNAxq7b',1,'2019-04-27 20:52:41','2019-04-27 20:52:41','2019-04-27 20:52:41'),(7,7,'L0e7llKGfJiHSQ03XfIkSYjQGI8CSRLy',1,'2019-04-27 21:33:04','2019-04-27 21:33:04','2019-04-27 21:33:04');
/*!40000 ALTER TABLE `activations` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `headings` WRITE;
/*!40000 ALTER TABLE `headings` DISABLE KEYS */;
INSERT INTO `headings` VALUES (1,'First Revenue Heading','FRH','2019-04-27 20:47:55','2019-04-27 20:47:55',1),(2,'Second revenue Heading','SRH','2019-04-27 20:48:13','2019-04-27 20:48:13',2),(3,'Third Revenue Heading','TRH','2019-04-27 20:48:35','2019-04-27 20:48:35',1),(4,'Fourth Revenue Heading','FtRH','2019-04-27 20:49:16','2019-04-27 20:49:16',2);
/*!40000 ALTER TABLE `headings` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `mdas` WRITE;
/*!40000 ALTER TABLE `mdas` DISABLE KEYS */;
INSERT INTO `mdas` VALUES (1,'Land & Infrastructure','LI','2019-04-27 20:47:08','2019-04-27 20:47:08'),(2,'Housing and Social Amenities','HSA','2019-04-27 20:47:31','2019-04-27 20:47:31');
/*!40000 ALTER TABLE `mdas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--


--
-- Table structure for table `persistences`
--

DROP TABLE IF EXISTS `persistences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistences`
--

LOCK TABLES `persistences` WRITE;
/*!40000 ALTER TABLE `persistences` DISABLE KEYS */;
INSERT INTO `persistences` VALUES (5,2,'h2xuKbqISWNVgaW9wzq0ogYaQCWqjzg0','2019-04-28 07:53:19','2019-04-28 07:53:19'),(6,2,'NgQ0ynY0peRNe6UJiFgs4xUjl3zkkHEW','2019-04-28 07:53:19','2019-04-28 07:53:19'),(7,2,'q9e9UIYXtRu5Av7ZQ2I0SaoARbnwQA1O','2019-04-28 07:53:19','2019-04-28 07:53:19'),(21,1,'BxamPT1YV41oaEZEA5AXKo31zGmHnTom','2019-05-04 06:35:42','2019-05-04 06:35:42'),(22,1,'YHGDBC7T5U8FGRFLEap3OARgziqURVNY','2019-05-04 06:35:43','2019-05-04 06:35:43'),(23,1,'OVroI2pk7QgtUQzSB5YWvIMNxakB6wcX','2019-05-04 06:35:43','2019-05-04 06:35:43');
/*!40000 ALTER TABLE `persistences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reminders`
--

LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS `role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_users`
--

LOCK TABLES `role_users` WRITE;
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;
INSERT INTO `role_users` VALUES (1,1,'2019-04-27 20:41:19','2019-04-27 20:41:19'),(2,2,'2019-04-27 20:41:19','2019-04-27 20:41:19'),(3,6,'2019-04-27 20:41:19','2019-04-27 20:41:19'),(4,7,'2019-04-27 20:41:20','2019-04-27 20:41:20'),(5,7,'2019-04-27 20:52:41','2019-04-27 20:52:41'),(7,7,'2019-04-27 21:33:04','2019-04-27 21:33:04');
/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'super-admin','Super Admin','{\"add-manage-mda\":true,\"add-manage-revenue-heading\":true,\"add-manage-sub-revenue-heading\":true,\"add-manage-sub-admin-users\":true,\"add-manage-desk-officers-and-other-users\":true,\"add-manage-tax-payers\":true,\"generate-assessment-and-payment\":true,\"edit-pending-assessment\":true,\"view-assessment-table\":true,\"view-transaction-table\":true,\"reports\":true}','2019-04-27 20:41:19','2019-04-27 20:41:19'),(2,'sub-admin','Sub Admin','{\"add-manage-mda\":false,\"add-manage-revenue-heading\":false,\"add-manage-sub-revenue-heading\":false,\"add-manage-sub-admin-users\":true,\"add-manage-desk-officers-and-other-users\":true,\"add-manage-tax-payers\":true,\"generate-assessment-and-payment\":true,\"edit-pending-assessment\":false,\"view-assessment-table\":true,\"view-transaction-table\":true,\"reports\":true}','2019-04-27 20:41:19','2019-04-27 20:41:19'),(3,'internal-revenue-staff','Internal Revenue Staff','{\"add-manage-mda\":false,\"add-manage-revenue-heading\":false,\"add-manage-sub-revenue-heading\":false,\"add-manage-sub-admin-users\":false,\"add-manage-desk-officers-and-other-users\":false,\"add-manage-tax-payers\":true,\"generate-assessment-and-payment\":true,\"edit-pending-assessment\":false,\"view-assessment-table\":true,\"view-transaction-table\":true,\"reports\":true}','2019-04-27 20:41:19','2019-04-27 20:41:19'),(4,'government-personel','Government Personel','{\"add-manage-mda\":false,\"add-manage-revenue-heading\":false,\"add-manage-sub-revenue-heading\":false,\"add-manage-sub-admin-users\":false,\"add-manage-desk-officers-and-other-users\":false,\"add-manage-tax-payers\":false,\"generate-assessment-and-payment\":false,\"edit-pending-assessment\":false,\"view-assessment-table\":false,\"view-transaction-table\":true,\"reports\":true}','2019-04-27 20:41:19','2019-04-27 20:41:19'),(5,'accounts','Accounts','{\"add-manage-mda\":false,\"add-manage-revenue-heading\":false,\"add-manage-sub-revenue-heading\":false,\"add-manage-sub-admin-users\":false,\"add-manage-desk-officers-and-other-users\":false,\"add-manage-tax-payers\":false,\"generate-assessment-and-payment\":false,\"edit-pending-assessment\":false,\"view-assessment-table\":false,\"view-transaction-table\":true,\"reports\":true}','2019-04-27 20:41:19','2019-04-27 20:41:19'),(6,'desk-officer','Desk Officer','{\"add-manage-mda\":false,\"add-manage-revenue-heading\":false,\"add-manage-sub-revenue-heading\":false,\"add-manage-sub-admin-users\":false,\"add-manage-desk-officers-and-other-users\":false,\"add-manage-tax-payers\":true,\"generate-assessment-and-payment\":true,\"edit-pending-assessment\":false,\"view-assessment-table\":true,\"view-transaction-table\":true,\"reports\":true}','2019-04-27 20:41:19','2019-04-27 20:41:19'),(7,'tax-payer','Tax Payer','{\"generate-assessment-and-payment\":true,\"edit-pending-assessment\":false,\"view-assessment-table\":true,\"view-transaction-table\":true,\"reports\":true}','2019-04-27 20:41:19','2019-04-27 20:41:19');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_headings`
--

DROP TABLE IF EXISTS `sub_headings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_headings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pricing` enum('fixed','not-fixed') COLLATE utf8mb4_unicode_ci NOT NULL,
  `heading_id` int(10) unsigned NOT NULL,
  `recurrent` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL,
  `validity` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `amount` decimal(8,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `sub_headings_heading_id_foreign` (`heading_id`),
  CONSTRAINT `sub_headings_heading_id_foreign` FOREIGN KEY (`heading_id`) REFERENCES `headings` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_headings`
--

LOCK TABLES `sub_headings` WRITE;
/*!40000 ALTER TABLE `sub_headings` DISABLE KEYS */;
INSERT INTO `sub_headings` VALUES (1,'Subheading One','SO','fixed',1,'yes',1,'2019-04-27 20:49:51','2019-04-27 20:49:51',12000.00),(2,'Second suubheading','SS','not-fixed',2,'yes',6,'2019-04-27 20:50:24','2019-04-27 20:50:24',NULL),(3,'Fixed Subheading','FS','fixed',3,'no',0,'2019-04-27 20:50:55','2019-04-27 20:50:55',15000.00),(4,'Non-fixed subheading','NFS','not-fixed',4,'no',0,'2019-04-27 20:51:23','2019-04-27 20:51:23',NULL);
/*!40000 ALTER TABLE `sub_headings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_payer_groups`
--

DROP TABLE IF EXISTS `tax_payer_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_payer_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shortcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_payer_groups`
--

LOCK TABLES `tax_payer_groups` WRITE;
/*!40000 ALTER TABLE `tax_payer_groups` DISABLE KEYS */;
INSERT INTO `tax_payer_groups` VALUES (1,'Individual Group','IDP','2019-04-27 20:46:00','2019-04-27 20:46:04','2019-04-27 20:46:04'),(2,'IDG','Individual Group','2019-04-27 20:46:23','2019-04-27 20:46:23',NULL),(3,'GG','General Group','2019-04-27 20:46:37','2019-04-27 20:46:37',NULL);
/*!40000 ALTER TABLE `tax_payer_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `throttle`
--

DROP TABLE IF EXISTS `throttle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `throttle`
--

LOCK TABLES `throttle` WRITE;
/*!40000 ALTER TABLE `throttle` DISABLE KEYS */;
/*!40000 ALTER TABLE `throttle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tranx_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tranx_ref` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gateway_response` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `authorization_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `taxpayer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_user_id_foreign` (`user_id`),
  CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `lga` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taxpayer_group_id` int(10) unsigned DEFAULT NULL,
  `occupation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_tax_payer_confirmed` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_taxpayer_group_id_foreign` (`taxpayer_group_id`),
  CONSTRAINT `users_taxpayer_group_id_foreign` FOREIGN KEY (`taxpayer_group_id`) REFERENCES `tax_payer_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'super@irms.com','$2y$10$zmXWQk40vxdKbpFEzcKT.uqWlvnhK4Ib46f8/m/t5dOCEbEg0cfYO',NULL,'2019-05-04 06:35:43','John Doe','1234567890',NULL,NULL,NULL,NULL,NULL,NULL,'0','2019-04-27 20:41:19','2019-05-04 06:35:43'),(2,'admin@irms.com','$2y$10$1Mjw1OkIQgYvT5GQM/Cvk.jS69So/0SdTr7dLp.3dFC3Yqd.vWuZ2',NULL,'2019-04-28 07:53:19','Susy Lee','1234567890',NULL,NULL,NULL,NULL,NULL,NULL,'0','2019-04-27 20:41:19','2019-04-28 07:53:19'),(3,'data@irms.com','$2y$10$9h7MXI4w7bdUO7kGrFs1E.RGF6ymaJ7hK.koP9gkzC.Fy6nOc54pC',NULL,NULL,'Jane Decker','1234567890',NULL,NULL,NULL,NULL,NULL,NULL,'0','2019-04-27 20:41:19','2019-04-27 20:41:19'),(4,'taxpayer@irms.com','$2y$10$PhqLefehIJrCMzwgIg0nsO9SRBcUEkFBvEDcpJjjmdfE5E1ZWeWeC',NULL,NULL,'Quincy Larson','1234567890',NULL,NULL,NULL,NULL,NULL,NULL,'0','2019-04-27 20:41:20','2019-04-27 20:41:20'),(7,'ugendu04@gmail.com','$2y$10$FVgqoRtC3tPGiP5dDSAAfuvbsGyL3.Y8vrLF3fbbr8tVDfbRNCUrG',NULL,'2019-05-04 06:17:11','Ugendu Martins Ositadinma','07062193996',NULL,'A11A busa buji street','Ajaokuta',NULL,NULL,'6SNfAWnOGb0','1','2019-04-27 21:33:04','2019-05-04 06:17:11');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-04 15:01:44
