$(function() {
  var updatesubHeadingDropdown = function() {
  if ($('#heading_dropdown').val() == '') {
      $('#subheading_dropdown').prop('disabled', true);
  } else {
      $('#subheading_dropdown').prop('disabled', false);
  }
};
  
updatesubHeadingDropdown();
  
  // disable submit button when clicked
  $("#submitBtn").on("click", function() {
      $("#submitBtn").addClass("disabled");
  });
  
  $('#heading_dropdown').change(function() {
    updatesubHeadingDropdown();
      // alert($('#programme_dropdown').val());
      $.ajax({
          "type":"GET",
          "url": "/ajaxcall/programme/sub-programme",
          "data": {
              "id": $('#heading_dropdown').val()
          },
          success: function(data) {
              $('#subheading_dropdown').empty();
              $("#subheading_dropdown").prepend("<option value='' selected='selected'>Sub-Heading</option>");
              console.log(data);
              $.each(data, function(i, subheading) {
                  $('#subheading_dropdown').append($("<option>").text(subheading['name']).attr('value', subheading['id']));
              });
              updatesubHeadingDropdown();
          },
          error: function(xhr, ajaxOptions, thrownError) {
              console.log(xhr.responseText);
          }
      });
  });
});