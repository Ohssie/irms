@extends('layouts.main')

@section('title')
  Sub-Headings
@endsection

@section('content')
  <div class="content-wrapper">
    <div class="row mb-4">
      <div class="col-12">
        @include('layouts.sessions')
      </div>
      <div class="col-12 d-flex align-items-center justify-content-between">
        <h4 class="page-title">Revenue SubHeadings</h4>
        <div class="d-flex align-items-center">
          <div class="wrapper mr-4 d-none d-sm-block">
            <p class="mb-0">Create a new
              <b class="mb-0">Revenue SubHeading</b>
            </p>
          </div>
          <div class="wrapper">
            <a href="#" class="btn btn-link btn-sm font-weight-bold">
              <button type="button" data-toggle="modal" data-target="#createSubHeading" class="btn btn-outline-success btn-fw"><i class="icon-plus"></i> New</button>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Revenue SubHeadings</h4>
        <div class="row">
          <div class="col-12 table-responsive">
            <table id="order-listing" class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Code</th>
                  <th>Pricing</th>
                  <th>Heading</th>
                  <th>Amount(&#x20A6;)</th>
                  <th>Recurrent?</th>
                  <th>Validity</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Code</th>
                  <th>Pricing</th>
                  <th>Heading</th>
                  <th>Amount(&#x20A6;)</th>
                  <th>Recurrent?</th>
                  <th>Validity</th>
                  <th>Actions</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($subs as $sub)
                <tr>
                  <td>{{ ucwords($sub->name) }}</td>
                  <td>{{ $sub->code }}</td>
                  <td>{{ ucwords($sub->pricing) }}</td>
                  @foreach($headings as $heading)
                  @if($heading->id == $sub->heading_id)
                  <td>{{ ucwords($heading->name) }}</td>
                  @endif
                  @endforeach
                  <td>&#x20A6;{{ number_format($sub->amount, 2) }}</td>
                  <td>{{ ucfirst($sub->recurrent) }}</td>
                  <td>{{ $sub->validity }} months</td>
                  <td>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit_subheading_{{$sub->id}}"><i class="icon-pencil"></i> Edit</button>
                    <button style="margin-left:5px;" type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_subheading_{{$sub->id}}"><i class="icon-trash"></i> Delete</button>
                  </td>
                </tr>
                @include('subheading.edit')
                @include('subheading.delete')
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('subheading.create')
@endsection

@section('scripts')
  <script src="{{ asset('assets/js/select2.js') }}"></script>
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
@endsection
