<div class="modal fade" id="edit_subheading_{{$sub->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="createGroupLabel">Edit Revenue SubHeading</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="forms-sample" method="post" action="{{ route('edit_subheading', $sub->id) }}">
        @csrf
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$sub->name}}" placeholder="Taxpayer Revenue Heading Code">
          </div>
          <div class="row">
            <div class="form-group col-6">
              <label for="code">Shortcode</label>
              <input type="text" class="form-control" name="code" id="code" value="{{$sub->code}}" placeholder="Enter Revenue Heading Code">
            </div>
            <div class="form-group col-6">
              <label>Pricing</label>
              <select class="form-control" name="pricing" required>
                <option selected>choose...</option>
                <option value="fixed">Fixed</option>
                <option value="not-fixed">Non-fixed</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-6">
              <label for="amount">Amount</label>
              <input type="text" class="form-control" name="amount" id="amount" placeholder="Example: 10000">
            </div>
            <div class="form-group col-6">
              <label>Heading</label>
              <select class="form-control" name="heading_id" required>
                <option value="" selected>choose...</option>
                @foreach($headings as $heading)
                <option value="{{$heading->id}}">{{ ucwords($heading->name) }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-6">
              <label>Recurrent?</label>
              <select class="form-control" name="recurrent" required>
                <option selected>choose...</option>
                <option value="yes">Yes</option>
                <option value="no">No</option>
              </select>
            </div>
            <div class="form-group col-6">
              <label>Validity</label>
              <select class="form-control" name="validity" required>
                <option selected>choose...</option>
                <option value="0">N/A (Not Applicable)</option>
                <option value="1">1 month</option>
                <option value="6">6 months</option>
                <option value="12">1 year</option>
                <option value="24">2 years</option>
              </select>
            </div>
          </div>          
        <!-- initial form closure position -->
      </div>
      <div class="modal-footer">
        <button type="submit" id="submit_form" class="btn btn-success mr-2">Submit</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>
  </div>
</div>