<div class="modal fade" id="edit_group_{{$group->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edit GroupLabel">Edit Taxpayer Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="forms-sample" method="POST" action="{{ route('edit-group', $group->id) }}">
        @csrf
          <div class="form-group">
            <label for="shortcode">Shortcode</label>
            <input type="text" class="form-control" name="shortcode" value="{{ $group->shortcode }}" id="shortcode" placeholder="Enter Taxpayer Group Shortcode" required>
          </div>
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="{{ $group->name }}" id="name" placeholder="Taxpayer Group Name" required>
          </div>
        <!-- initial form closure position -->
      </div>
      <div class="modal-footer">
        <button type="submit" id="submit_form" class="btn btn-success mr-2">Submit</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>
  </div>
</div>