<div class="modal fade" id="delete_group_{{$group->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edit GroupLabel">Delete Taxpayer Group?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="s-text">Remove <strong class="modal-title">'{{ ucwords($group->name) }}'</strong> from your list of Tax Payers Group? </br><span class="p-text">This cannot be undone.</span></p>
      </div>
      <div class="modal-footer">
        <!-- <a href="" class="btn btn-fill btn-danger pull-right">Delete</a> -->
        <form method="get" action="{{route('delete-group', $group->id)}}">
        <button type="submit" id="submit_form" class="btn btn-danger mr-2">Delete</button>
        </form>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>