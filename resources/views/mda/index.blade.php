@extends('layouts.main')

@section('title')
  MDAs
@endsection

@section('content')
  <div class="content-wrapper">
    <div class="row mb-4">
      <div class="col-12">
        @include('layouts.sessions')
      </div>
      <div class="col-12 d-flex align-items-center justify-content-between">
        <h4 class="page-title">MDAs</h4>
        <div class="d-flex align-items-center">
          <div class="wrapper mr-4 d-none d-sm-block">
            <p class="mb-0">Create a new
              <b class="mb-0">MDA</b>
            </p>
          </div>
          <div class="wrapper">
            <a href="#" class="btn btn-link btn-sm font-weight-bold">
              <button type="button" data-toggle="modal" data-target="#createMda" class="btn btn-outline-success btn-fw"><i class="icon-plus"></i> New</button>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 card-statistics">
        <div class="row">
          @foreach($mdas as $mda)
          <div class="col-12 col-sm-6 col-md-3 grid-margin stretch-card card-tile">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between pb-2">
                  <h5>{{ strtoupper($mda->code) }}</h5>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="text-muted">{{ ucwords($mda->name) }}</p>
                </div>
                <br/>
                <div class="row" style="margin:auto; text-align:center;">
                  <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit_mda_{{$mda->id}}"><i class="icon-pencil"></i> Edit</button>
                  <button style="margin-left:5px;" type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete_mda_{{$mda->id}}"><i class="icon-trash"></i> Delete</button>
                </div>
              </div>
            </div>
          </div>
          @include('mda.edit')
          @include('mda.delete')
          @endforeach
        </div>
      </div>
    </div>
  </div>
  @include('mda.create')
@endsection

@section('scripts')
  

@endsection
