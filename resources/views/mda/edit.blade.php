<div class="modal fade" id="edit_mda_{{$mda->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edit GroupLabel">Edit MDA</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="forms-sample" method="POST" action="{{ route('edit_mda', $mda->id) }}">
        @csrf
          <div class="form-group">
            <label for="shortcode">Shortcode</label>
            <input type="text" class="form-control" name="code" value="{{ $mda->code }}" id="code" placeholder="Enter MDA code" required>
          </div>
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="{{ $mda->name }}" id="name" placeholder="MDA Name" required>
          </div>
        <!-- initial form closure position -->
      </div>
      <div class="modal-footer">
        <button type="submit" id="submit_form" class="btn btn-success mr-2">Submit</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>
  </div>
</div>