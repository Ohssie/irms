@extends('layouts.master')

@section('title')
  Manage Users
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('plugins/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('content')
    <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Entry Personel</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item">Data Entry Personels</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        @include('layouts.sessions')
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Entry Personels</h4>
                                <h6 class="card-subtitle">List of Data Entry Personels</h6>
                                <button class="btn btn-success" style="float:right;" data-toggle="modal" data-target="#create-data-entry-modal"><i class="ti-plus"></i> Add User</button>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>
                                              <th>Full Name</th>
                                              <th>Email</th>
                                              <th>Phone Number</th>
                                              <th class="text-nowrap">Action</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($users as $user)
                                        <tr>
                                              <td>{{$user->full_name}}</td>
                                              <td>{{$user->email}}</td>
                                              <td>{{$user->phone_number}}</td>
                                              <td>
                                                  <button class="btn waves-effect waves-light btn-sm btn-info" data-toggle="modal" data-target="#edit_user_{{$user->id}}">Edit</button>
                                                  <button class="btn waves-effect waves-light btn-sm btn-danger" data-toggle="modal" data-target="#delete_user_{{$user->id}}">Delete</button>
                                              </td>
                                        </tr>
                                        @include('admin.edit-data-entry-personel')
                                        @include('admin.delete-data-entry-personel')
                                        @endforeach
                                      </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('admin.create-data-entry-personel')

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            @include('layouts.footer')
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
@endsection

@include('layouts.datatables')
@include('layouts.imagepicker')
