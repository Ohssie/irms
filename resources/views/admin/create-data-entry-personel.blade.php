<div id="create-data-entry-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Create Data Entry Personel</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        {{Form::open(['route' => 'store-data-entry-personel', 'method' => 'POST', 'class' => 'm-form', 'enctype' => 'multipart/form-data'])}}
        @csrf
          <div class="form-group">
            <label for="heading-name" class="control-label">Name:</label>
            <input type="text" class="form-control" name="name" required>
          </div>
          <div class="form-group">
            <label for="heading-code" class="control-label">Email:</label>
            <input type="email" class="form-control" name="email" required>
          </div>
          <div class="form-group">
            <label for="heading-code" class="control-label">Phone Number:</label>
            <input type="text" class="form-control" name="phone_number" required>
          </div>
          {{-- <div class="form-group">
              <label for="image" class="control-label">Add Image:</label>
              <input type="file" class="form-control" id="image" name="avatar">
          </div> --}}
          <div class="row">
            <div class="form-group col-md-12">
              <label for="input-file-max-fs">Add Image:</label>
              <input type="file" id="input-file-max-fs" name="avatar" class="dropify" data-max-file-size="2M" />
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-12">
              <label for="image" class="control-label">Station/Location:</label>
              <select class="form-control" name="lga">
                <option value="">--Select Location--</option>
                @foreach(config('lga.kogi_lgas') as $lga)
                  <option value="{{$lga}}">{{$lga}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <input type="hidden" name="user_role" value="data-entry">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
      </div>
      {{Form::close()}}
    </div>
  </div>
</div>
