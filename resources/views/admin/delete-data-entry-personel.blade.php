<!-- Delete Modal -->
<div class="modal fade m-medium" id="delete_user_{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-delete-heading">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Delete Data Entry Personel?</h4>
            </div>
            <div class="modal-body">
                <p class="s-text">Remove <strong class="modal-title">'{{ ucwords($user->full_name) }}'</strong> from your list of Data Entry Personels? </br><span class="p-text">This cannot be undone.</span></p>
            </div>
            <div class="modal-footer">
                <a href="{{route('delete-data-entry', $user->id)}}" class="btn btn-fill btn-danger pull-right">Delete</a>
            </div>
        </div>
    </div>
</div>
