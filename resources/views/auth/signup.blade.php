@extends('auth.layout')

@section('title')
  Signup
@endsection

@section('content')
<div class="row d-flex align-items-stretch">
  <div class="col-md-4 banner-section d-none d-md-flex align-items-stretch justify-content-center">
    <div class="slide-content bg-2">
    </div>
  </div>
  <div class="col-12 col-md-8 h-100 bg-white">
    <div class="auto-form-wrapper d-flex align-items-center justify-content-center flex-column">
      <div class="nav-get-started">
        <p>Already have an account?</p>
        <a class="btn get-started-btn" href="{{route('get_login')}}">SIGN IN</a>
      </div>
      <form action="{{route('handle_signup')}}" method="post">
        @csrf
        <h3 class="mr-auto">Register</h3>
        <p class="mb-5 mr-auto">Enter your details below.</p>
        @include('layouts.sessions')
        <div class="row">
          <div class="form-group col-12">
            <input class="form-control" name="full_name" type="text" required="" placeholder="Name" value="{{ old('full_name') }}">
          </div>
          <div class="form-group col-6">
            <input class="form-control" name="email" type="email" required="" placeholder="Email" value="{{ old('email') }}">
          </div>
          <div class="form-group col-6">
            <select class="form-control js-example-basic-single w-100" name="lga">
              <option>Location</option>
              @foreach(config('lga.kogi_lgas') as $lga)
                <option value="{{$lga}}">{{$lga}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-6">
            <select class="form-control" name="occupation">
              <option>Occupation</option>
              @foreach(config('lga.occupations') as $occupation)
                <option value="{{$occupation}}">{{$occupation}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-6">
            <select class="form-control" name="tax_payer_group_id">
              <option>Group</option>
              @foreach($groups as $group)
                <option value="{{$group->id}}">{{ ucfirst($group->name) }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-12">
            <input class="form-control" name="phone_number" type="text" required="" placeholder="Phone Number" value="{{ old('phone_number') }}">
          </div>
        </div>

        <div class="row">
          <div class="form-group col-12">
            <textarea class="form-control" name="address" placeholder="Address" required>{{ old('address') }}</textarea>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-6">
            <input type="password" class="form-control" placeholder="Password" name="password" required="">
          </div>
          <div class="form-group col-6">
            <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required="">
          </div>
        </div>

        <div class="form-group">
          <button class="btn btn-primary submit-btn">SIGN UP</button>
        </div>
        <div class="form-group">
          <p>Want to make a Quick service payment?</p>  
          <a class="btn btn-outline-dark btn-rounded btn-fw" href="{{route('quick_payment')}}">Make Payment</a>
        </div>

        <div class="wrapper mt-5 text-gray" style="margin-bottom:60px;">
          <p class="footer-text">Copyright © <?php echo date('Y'); ?> Opateq Consulting. All rights reserved.</p>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
