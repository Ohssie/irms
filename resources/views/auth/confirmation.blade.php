@extends('auth.layout')

@section('title')
  Confirmation
@endsection

@section('content')
<div class="row d-flex align-items-stretch">
  <div class="col-md-4 banner-section d-none d-md-flex align-items-stretch justify-content-center">
    <div class="slide-content bg-1">
    </div>
  </div>
  <div class="col-12 col-md-8 h-100 bg-white">
    <div class="auto-form-wrapper d-flex align-items-center justify-content-center flex-column">
      <div class="nav-get-started">
        <p>Already have an account?</p>
        <a class="btn get-started-btn" href="{{route('get_login')}}">Sign In</a>
      </div>
      <form action="{{route('confirm-user')}}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <h3 class="mr-auto">Hello! {{$user->full_name}}</h3>
        <p class="mb-5 mr-auto">Enter the verification code sent to your phone.</p>
        @include('layouts.sessions')
        <div class="form-group">
          <input type="text" class="form-control" required="" placeholder="Verification Code" name="confirmation_code">
        </div>
        <input type="hidden" name="user_id" value="{{$user->id}}">

        <div class="form-group">
          <button class="btn btn-primary submit-btn" type="submit">Verify Account</button>
        </div>
        <small>Didn't get the confirmation message? <a href="{{route('resend-confirmation-code', \Crypt::encryptString($user->id))}}"> Resend Verification Code </a> </small>
        <div class="wrapper mt-5 text-gray">
          <p class="footer-text">Copyright © <?php echo date('Y'); ?> Opateq Consulting. All rights reserved.</p>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
