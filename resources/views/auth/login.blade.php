@extends('auth.layout')

@section('title')
  Login
@endsection

@section('content')
<div class="row d-flex align-items-stretch">
  <div class="col-md-4 banner-section d-none d-md-flex align-items-stretch justify-content-center">
    <div class="slide-content bg-1">
    </div>
  </div>
  <div class="col-12 col-md-8 h-100 bg-white">
    <div class="auto-form-wrapper d-flex align-items-center justify-content-center flex-column">
      <div class="nav-get-started">
        <p>Don't have an account?</p>
        <a class="btn get-started-btn" href="{{route('sign_up')}}">GET STARTED</a>
      </div>
      <form action="{{route('post_login')}}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <h3 class="mr-auto">Hello! let's get started</h3>
        <p class="mb-5 mr-auto">Enter your details below.</p>
        @include('layouts.sessions')
        <div class="form-group">
          <input type="text" class="form-control" required="" placeholder="Email Address" name="email">
        </div>
        <div class="form-group">
          <input type="password" class="form-control" required="" placeholder="Password" name="password">
        </div>
        <div class="form-group">
          <button class="btn btn-primary submit-btn" type="submit">SIGN IN</button>
        </div>
        <div class="wrapper mt-5 text-gray">
          <p class="footer-text">Copyright © <?php echo date('Y'); ?> Opateq Consulting. All rights reserved.</p>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
