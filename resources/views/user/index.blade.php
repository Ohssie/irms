@extends('layouts.main')

@section('title')
  Users
@endsection

@section('content')
  <div class="content-wrapper">
    <div class="row mb-4">
      <div class="col-12">
        @include('layouts.sessions')
      </div>
      <div class="col-12 d-flex align-items-center justify-content-between">
        <h4 class="page-title">Users</h4>
        @if(Sentinel::hasAccess('add-manage-sub-revenue-heading'))
        <div class="d-flex align-items-center">
          <div class="wrapper mr-4 d-none d-sm-block">
            <p class="mb-0">Create a new
              <b class="mb-0">User</b>
            </p>
          </div>
          <div class="wrapper">
            <a href="#" class="btn btn-link btn-sm font-weight-bold">
              <button type="button" data-toggle="modal" data-target="#createUser" class="btn btn-outline-success btn-fw"><i class="icon-plus"></i> New</button>
            </a>
          </div>
        </div>
        @endif
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Users</h4>
        <div class="row">
          <div class="col-12 table-responsive">
            <table id="order-listing" class="table">
              <thead>
                <tr>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Role</th>
                  @if(Sentinel::hasAccess('add-manage-sub-revenue-heading'))
                  <th>Actions</th>
                  @endif
                </tr>
              </thead>
              <tbody>
                @foreach($users as $user)
                <tr>
                  <td>{{$user->full_name}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->phone_number}}</td>
                  <td>
                    @foreach ($user->roles as $key => $role)
                        <small>{{$role->name}}</small>
                    @endforeach
                  </td>
                  @if(Sentinel::hasAccess('add-manage-sub-revenue-heading'))
                  <td>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit_user_{{$user->id}}"><i class="icon-pencil"></i> Edit</button>
                    <button style="margin-left:5px;" type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_user_{{$user->id}}"><i class="icon-trash"></i> Delete</button>
                  </td>
                  @endif
                </tr>
                @include('user.edit')
                @include('user.delete')
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('user.create')
@endsection

@section('scripts')
  <script src="{{ asset('assets/js/select2.js') }}"></script>
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
  <script src="{{ asset('assets/js/file-upload.js') }}"></script>
@endsection
