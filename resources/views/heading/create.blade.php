<div class="modal fade" id="createHeading" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="createGroupLabel">Create Heading</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="forms-sample" method="post" action="{{ route('store_heading') }}">
        @csrf
          <div class="form-group">
            <label for="code">Shortcode</label>
            <input type="text" class="form-control" name="code" id="code" placeholder="Enter Revenue Heading Code">
          </div>
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Taxpayer Revenue Heading Code">
          </div>
          <div class="form-group">
            <label>MDA</label>
            <select class="form-control" name="mda_id" required>
              <option value="" selected>choose...</option>
              @foreach($mdas as $mda)
              <option value="{{ $mda->id }}">{{ $mda->name }}</option>
              @endforeach
            </select>
          </div>
        <!-- initial form closure position -->
      </div>
      <div class="modal-footer">
        <button type="submit" id="submit_form" class="btn btn-success mr-2">Submit</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>
  </div>
</div>