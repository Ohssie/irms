<div class="modal fade" id="edit_heading_{{$heading->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edit GroupLabel">Edit Heading - <b>{{ $heading->name }}</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="forms-sample" method="POST" action="{{ route('edit_heading', $heading->id) }}">
        @csrf
          <div class="form-group">
            <label for="shortcode">Shortcode</label>
            <input type="text" class="form-control" name="code" value="{{ $heading->code }}" id="code" placeholder="Enter Heading code" required>
          </div>
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="{{ $heading->name }}" id="name" placeholder="Heading Name" required>
          </div>
          <div class="form-group">
            <label>MDA</label>
            <select class="form-control" name="mda_id" required>
              <option value="" selected>choose...</option>
              <?php $selectedvalue=$heading->mda_id ?>
              @foreach($mdas as $mda)
              <option value="{{ $mda->id }}" {{ $selectedvalue == $mda->id ? 'selected="selected"' : '' }}>{{ $mda->name }}</option>
              @endforeach
            </select>
          </div>
        <!-- initial form closure position -->
      </div>
      <div class="modal-footer">
        <button type="submit" id="submit_form" class="btn btn-success mr-2">Submit</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>
  </div>
</div>