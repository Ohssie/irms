@extends('layouts.main')

@section('title')
  Headings
@endsection

@section('content')
  <div class="content-wrapper">
    <div class="row mb-4">
      <div class="col-12">
        @include('layouts.sessions')
      </div>
      <div class="col-12 d-flex align-items-center justify-content-between">
        <h4 class="page-title">Revenue Headings</h4>
        <div class="d-flex align-items-center">
          <div class="wrapper mr-4 d-none d-sm-block">
            <p class="mb-0">Create a new
              <b class="mb-0">Revenue Heading</b>
            </p>
          </div>
          <div class="wrapper">
            <a href="#" class="btn btn-link btn-sm font-weight-bold">
              <button type="button" data-toggle="modal" data-target="#createHeading" class="btn btn-outline-success btn-fw"><i class="icon-plus"></i> New</button>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Revenue Headings</h4>
        <div class="row">
          <div class="col-12 table-responsive">
            <table id="order-listing" class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Code</th>
                  <th>MDA</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($headings as $heading)
                <tr>
                  <td>{{ ucwords($heading->name) }}</td>
                  <td>{{ strtoupper($heading->code) }}</td>
                  @foreach($mdas as $mda)
                  @if($mda->id == $heading->mda_id)
                  <td>{{ ucwords($mda->name) }}</td>
                  @endif
                  @endforeach
                  <td>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit_heading_{{$heading->id}}"><i class="icon-pencil"></i> Edit</button>
                    <button style="margin-left:5px;" type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_heading_{{$heading->id}}"><i class="icon-trash"></i> Delete</button>
                  </td>
                </tr>
                @include('heading.edit')
                @include('heading.delete')
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('heading.create')
@endsection

@section('scripts')
  <script src="{{ asset('assets/js/select2.js') }}"></script>
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
@endsection
