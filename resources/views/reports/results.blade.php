@extends('layouts.master')

@section('title')
  Manage Tax Payer Groups
@stop

@section('content')
    <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Reports</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('get-reports')}}">Back to query</a></li>
                        <li class="breadcrumb-item">Results</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        @include('layouts.sessions')
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                              @if(isset($tax_payers))
                                  <div class="col-md-4">
                                    <h4 class="card-title">Tax Payers Reports</h4>
                                    <!--<h6 class="card-subtitle">Tax Payers Reports</h6>-->
                                  </div>
                                  <div class="table-responsive m-t-40">
                                      <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Full Name</th>
                                                <th>Unique ID</th>
                                                <th>Phone Number</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($tax_payers as $tax_payer)
                                          <tr>
                                              <td>{{$tax_payer->full_name}}</td>
                                              <td>{{$tax_payer->unique_id}}</td>
                                              <td>{{$tax_payer->phone_number}}</td>
                                          </tr>
                                          @endforeach
                                        </tbody>
                                      </table>
                                  </div>
                              @endif
                              @if(isset($headings))
                                  <div class="col-md-4">
                                    <h4 class="card-title">Heading Reports</h4>
                                    <!--<h6 class="card-subtitle">Heading Reports</h6>-->
                                  </div>
                                  <div class="table-responsive m-t-40">
                                      <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>code</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($headings as $heading)
                                          <tr>
                                              <td>{{$heading->name}}</td>
                                              <td>{{$heading->code}}</td>
                                          </tr>
                                          @endforeach
                                        </tbody>
                                      </table>
                                  </div>
                              @endif
                              @if(isset($sub_headings))
                                  <div class="col-md-4">
                                    <h4 class="card-title">Sub Heading Reports</h4>
                                    <!--<h6 class="card-subtitle">Sub Heading Reports</h6>-->
                                  </div>
                                  <div class="table-responsive m-t-40">
                                      <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Pricing</th>
                                                <th>Amount</th>
                                                <th>Validity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($sub_headings as $sub_heading)
                                          <tr>
                                              <td>{{$sub_heading->name}}</td>
                                              <td>{{$sub_heading->code}}</td>
                                              <td>{{$sub_heading->pricing}}</td>
                                              <td>{{$sub_heading->amount}}</td>
                                              <td>{{$sub_heading->validity}}</td>
                                          </tr>
                                          @endforeach
                                        </tbody>
                                      </table>
                                  </div>
                              @endif
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            @include('layouts.footer')
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
@endsection
@include('layouts.datatables')
