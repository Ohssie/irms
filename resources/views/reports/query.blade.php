@extends('layouts.master')

@section('title')
  Reports
@stop

@section('styles')
  <link href="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
  <style media="screen">
    #month{
      display: none;
    }
    #day{
      display: none;
    }
  </style>
@endsection

@section('content')
    <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Reports</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item">Reports</li>
                    </ol>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        @include('layouts.sessions')
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card card-outline-info">
                                            <div class="card-header">
                                                <h4 class="m-b-0 text-white">Reports</h4>
                                            </div>
                                            <div class="card-body">
                                                <form action="{{route('reports')}}" class="form-horizontal form-bordered" method="post">
                                                  {{ csrf_field() }}
                                                    <div class="form-body">
                                                        <div class="form-group row">
                                                            <label class="control-label text-right col-md-3">Category</label>
                                                            <div class="col-md-9">
                                                                <select class="form-control custom-select" name="category">
                                                                  <option value="">--select category--</option>
                                                                  @foreach(config('reports.categories') as $category)
                                                                    <option value="{{$category['value']}}">{{$category['name']}}</option>
                                                                  @endforeach
                                                                </select>
                                                                <small class="form-control-feedback"> Choose a category </small> </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="control-label text-right col-md-3">Filters</label>
                                                            <div class="col-md-9">
                                                              <select class="form-control custom-select" id="filter">
                                                                <option value="">--filter--</option>
                                                                @foreach(config('reports.filters') as $filter)
                                                                  <option value="{{$filter['value']}}">{{$filter['name']}}</option>
                                                                @endforeach
                                                              </select>
                                                            </div>
                                                        </div>
                                                        <div class="" id="month">
                                                          <div class="form-group row">
                                                              <label class="control-label text-right col-md-3">Months</label>
                                                              <div class="col-md-9">
                                                                <select class="form-control custom-select" name="month">
                                                                  <option value="">--Months--</option>
                                                                  @foreach(config('reports.months') as $month)
                                                                    <option value="{{$month['name']}}|{{$month['limit']}}|{{$loop->iteration}}">{{$month['name']}}</option>
                                                                  @endforeach
                                                                </select>
                                                              </div>
                                                          </div>
                                                          <div class="form-group row">
                                                              <label class="control-label text-right col-md-3"></label>
                                                              <div class="col-md-9">
                                                                <select class="form-control custom-select" name="year">
                                                                  <option value="">--Years--</option>
                                                                  @foreach(config('reports.years') as $year)
                                                                    <option value="{{$year}}">{{$year}}</option>
                                                                  @endforeach
                                                                </select>
                                                              </div>
                                                          </div>
                                                        </div>
                                                        <div class="" id="day">
                                                          <div class="form-group row">
                                                              <label class="control-label text-right col-md-3">Date</label>
                                                              <div class="col-md-9">
                                                                <input type="date" class="form-control mydatepicker" name="day" placeholder="mm/dd/yyyy">
                                                              </div>
                                                          </div>
                                                        </div>

                                                      <div class="form-actions">
                                                          <div class="row">
                                                              <div class="col-md-12">
                                                                  <div class="row">
                                                                      <div class="offset-sm-3 col-md-9">
                                                                          <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            @include('layouts.footer')
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
@endsection
@section('js')
<script>
    $("#filter").on('change', function() {
        var value = $(this).val();
        if (value == 'month'){
          $("#month").show();
          $("#day").hide();
        }
        if (value == 'day'){
          $("#day").show();
          $("#month").hide();
        }
    });
</script>
@endsection
