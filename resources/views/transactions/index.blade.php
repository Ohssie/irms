@extends('layouts.main')

@section('title')
  Transactions
@endsection

@section('content')
  <div class="content-wrapper">
    <div class="row mb-4">
      <div class="col-12">
        @include('layouts.sessions')
      </div>
      <div class="col-12 d-flex align-items-center justify-content-between">
        <h4 class="page-title">Transactions</h4>
        <!-- <div class="d-flex align-items-center">
          <div class="wrapper mr-4 d-none d-sm-block">
            <p class="mb-0">Create a new
              <b class="mb-0">Revenue SubHeading</b>
            </p>
          </div>
          <div class="wrapper">
            <a href="#" class="btn btn-link btn-sm font-weight-bold">
              <button type="button" data-toggle="modal" data-target="#createSubHeading" class="btn btn-outline-success btn-fw"><i class="icon-plus"></i> New</button>
            </a>
          </div>
        </div> -->
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Transactions</h4>
        <div class="row">
          <div class="col-12 table-responsive">
            <table id="order-listing" class="table">
              <thead>
                <tr>
                  <th>Transaction ID</th>
                  <th>Transaction Reference</th>
                  <th>Response</th>
                  <th>Amount(&#x20A6;)</th>
                  <th>Taxpayer</th>
                  <th>User</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Transaction ID</th>
                  <th>Transaction Reference</th>
                  <th>Response</th>
                  <th>Amount(&#x20A6;)</th>
                  <th>Taxpayer</th>
                  <th>User</th>
                  <th>Date</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($tranx as $tran)
                <tr>
                  <td>{{$tran->tranx_id}}</td>
                  <td>{{$tran->tranx_ref}}</td>
                  <td>{{$tran->gateway_response}}</td>
                  <td>&#x20A6;{{number_format(($tran->amount/100), 2) }}</td>
                  @foreach($users as $payer)
                  @if($payer->id == $tran->taxpayer)
                    <td>{{ ucwords($payer->full_name) }}</td>
                  @endif
                  @endforeach
                  @foreach($users as $user)
                  @if($user->id == $tran->user_id)
                    <td>{{ ucwords($user->full_name) }}</td>
                  @endif
                  @endforeach
                  <td>{{ date_format($tran->created_at, 'l jS F Y') }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ asset('assets/js/select2.js') }}"></script>
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
@endsection
