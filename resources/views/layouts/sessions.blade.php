@if (session('info'))
  <div class="col-md-12">
    <div class="alert alert-fill-primary alert-dismissible fade show" role="alert">
      <i class="icon-info"></i>
      <span>{{session('info')}}</span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
@endif

@if (session('success'))
  <div class="col-md-12">
    <div class="alert alert-fill-success alert-dismissible fade show" role="alert">
      <i class="icon-info"></i>
      <span>{{session('success')}}</span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
@endif

@if (session('attention'))
  <div class="col-md-12">
    <div class="alert alert-fill-info alert-dismissible fade show" role="alert">
      <i class="icon-info"></i>
      <span>{{session('attention')}}</span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
@endif

@if (session('warning'))
  <div class="col-md-12">
    <div class="alert alert-fill-warning alert-dismissible fade show" role="alert">
      <i class="icon-info"></i>
      <span>{{session('warning')}}</span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
@endif

@if (session('error'))
  <div class="col-md-12">
    <div class="alert alert-fill-danger alert-dismissible fade show" role="alert">
      <i class="icon-info"></i>
      <span>{{session('error')}}</span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
@endif

@if ($errors->any())
  <div class="col-md-12">
    <div class="alert alert-fill-warning alert-dismissible fade show" role="alert">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        @endforeach
      </ul>
    </div>
  </div>
@endif
