<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas sidebar-dark" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile" style="margin-top:15px;">
      <img src="{{ asset('images/iGRMS-favicon.png')}}" alt="profile image">
      <p class="text-center font-weight-medium">Larry Garner</p>
    </li>
    @if(!Sentinel::guest())
      <li class="nav-item">
        <a class="nav-link" href="{{route('user_dashboard')}}">
          <i class="menu-icon icon-diamond"></i>
          <span class="menu-title">Dashboard</span>
          <div class="badge badge-success">3</div>
        </a>
      </li>
      @if(Sentinel::hasAccess('add-manage-desk-officers-and-other-users'))
       <li class="nav-item">
         <a class="nav-link" href="{{route('manage_users')}}">
           <i class="menu-icon icon-people"></i>
           <span class="menu-title">Users</span>
         </a>
       </li>
      @endif
      @if(!Sentinel::getUser()->inRole('tax-payer'))
        <li class="nav-item">
          <a class="nav-link" href="{{route('tax_payers')}}">
            <i class="menu-icon icon-people"></i>
            <span class="menu-title">Tax Payers</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('manage-group') }}">
            <i class="menu-icon icon-list"></i>
            <span class="menu-title">Tax Payer Groups</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('mdas') }}">
            <i class="menu-icon icon-list"></i>
            <span class="menu-title">MDAs</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('headings') }}">
            <i class="menu-icon icon-list"></i>
            <span class="menu-title">Revenue Headings</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('sub_headings') }}">
            <i class="menu-icon icon-list"></i>
            <span class="menu-title">Revenue Sub-Headings</span>
          </a>
        </li>
      @endif

      @if(Sentinel::hasAccess('view-assessment-table'))
        <li class="nav-item">
          <a class="nav-link" href="{{route('invoices')}}">
            <i class="menu-icon icon-notebook"></i>
            <span class="menu-title">Invoices</span>
          </a>
        </li>
      @endif
      @if(Sentinel::hasAccess('view-transaction-table'))
        <li class="nav-item">
          <a class="nav-link" href="{{ route('transactions') }}">
            <i class="menu-icon icon-credit-card"></i>
            <span class="menu-title">Transactions</span>
          </a>
        </li>
      @endif
      <li class="nav-item">
        <a class="nav-link" href="{{ route('assessments') }}">
          <i class="menu-icon icon-credit-card"></i>
          <span class="menu-title">Assessments</span>
        </a>
      </li>
      @if ((!Sentinel::getUser()->inRole('tax-payer')))
        <li class="nav-item">
          {{-- <a class="nav-link" href="{{route('get-reports')}}">--}}
          <a class="nav-link" href="#">
            <i class="menu-icon icon-drawer"></i>
            <span class="menu-title">Reports</span>
          </a>
        </li>
      @endif
    @endif
  </ul>
</nav>
