<div class="modal fade" id="edit_user_{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edit GroupLabel">Edit Taxpayer - <b>{{ $user->name }}</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="forms-sample" method="POST" action="{{ route('edit-user', $user->id) }}" enctype="multipart/form-data">
        @csrf
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="{{ $user->full_name }}" id="name" placeholder="Full Name" required>
          </div>
          <div class="form-group">
            <label for="email">Email Address</label>
            <input type="text" class="form-control" name="email" value="{{ $user->email }}" id="email" placeholder="Email Address" required>
          </div>
          <div class="form-group">
            <label for="phone_number">Phone Number</label>
            <input type="text" class="form-control" name="phone_number" value="{{ $user->phone_number }}" id="phone_number" placeholder="Phone Number" required>
          </div>
          <div class="form-group">
            <label>Role</label>
            <select class="form-control" name="role" required>
              <option value="">Choose role...</option>
              <?php $selectedvalue=$user->role ?>
              @foreach($roles as $role)
              <option value="{{$role->slug}}"
                    @if ($user->roles->first()->id == $role->id)
                      selected
                    @endif
                  >{{$role->name}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label>Image upload</label>
            <input type="file" name="avatar[]" class="file-upload-default">
            <div class="input-group col-xs-12">
              <input type="text" class="form-control file-upload-info" data-max-file-size="2M" data-default-file="{{$user->avatar}}" disabled placeholder="Upload Image">
              <span class="input-group-append">
                <button class="file-upload-browse btn btn-info" type="button">Upload</button>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label>Station/Location</label>
            <select class="form-control" name="lga" required>
              <option value="">--Select Location--</option>
              @foreach(config('lga.kogi_lgas') as $lga)
                <option value="{{$lga}}"
                  @if($user->plateau_lga == $lga)
                    selected
                  @endif
                >{{$lga}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label>MDA</label>
            <select class="form-control" name="mdas[]" required>
              <option value="">--Select MDA(s)</option>
              @foreach($mdas as $mda)
                <option value="{{$mda->id}}"
                  {{-- $collection->contains('Desk'); --}}
                  @if($user->mdas->contains($mda))
                    selected
                  @endif
                  >{{$mda->name}}
                </option>
              @endforeach
            </select>
          </div>
        <!-- initial form closure position -->
      </div>
      <div class="modal-footer">
        <button type="submit" id="submit_form" class="btn btn-success mr-2">Submit</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
        </form>
    </div>
  </div>
</div>