@extends('layouts.main')

@section('title')
Assessment
@endsection

@section('content')
  <div class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card px-2">
          <div class="card-body">
            <div class="container-fluid">
              <h3 class="text-right my-5">Invoice</h3>
              <hr>
            </div>
            <div class="container-fluid d-flex justify-content-between">
              <div class="col-lg-3 pl-0">
                <p class="mt-5 mb-2">
                  <b>Less Expenses</b>
                  <a href="#!" id="addExpenseItem" class="btn btn-outline-success btn-fw float-right">
                    <i class="icon-plus"></i>
                  </a>
                </p>
              </div>
            </div>

            <form class="" action="{{route('store-assessment')}}" method="post">
              <div id="less_expenses_div">
                <div class="row mt-3">
                  <div class="col-md-6">
                    <label for="">Expense Item</label>
                    <input class="form-control" type="text" name="less_expenses_item[]" value="">
                  </div>
                  <div class="col-md-6">
                    <label for="">Amount</label>
                    <input class="form-control" type="text" name="less_expenses_amount[]" value="">
                  </div>
                </div>
              </div>

              <p class="mt-5 mb-2">
                <b>Employee Income (Salary, Wages, Commission, Bonus, Gratuities)</b>
                <a href="#!" id="addEmployeeIncome" class="btn btn-outline-success btn-fw float-right">
                  <i class="icon-plus"></i>
                </a>
              </p>
              <div id="employee_income_div">
                <div class="row mt-3">
                  <div class="col-md-6">
                    <label for="">Item</label>
                    <input class="form-control" type="text" name="employee_income_item[]" value="">
                  </div>
                  <div class="col-md-6">
                    <label for="">Amount</label>
                    <input class="form-control" type="text" name="employee_income_amount[]" value="">
                  </div>
                </div>
              </div>

              <p class="mt-5 mb-2">
                <b>Other Income (Rent, Interest or Discount, Charge or Annuity)</b>
                <a href="#!" id="addOtherIncome" class="btn btn-outline-success btn-fw float-right">
                  <i class="icon-plus"></i>
                </a>
              </p>
              <div id="other_income_div">
                <div class="row mt-3">
                  <div class="col-md-6">
                    <label for="">Item</label>
                    <input class="form-control" type="text" name="other_income_item[]" value="">
                  </div>
                  <div class="col-md-6">
                    <label for="">Amount</label>
                    <input class="form-control" type="text" name="other_income_amount[]" value="">
                  </div>
                </div>
              </div>

              <p class="mt-5 mb-2">
                <b>Less Relief, Non-Taxable Income and Advance Final Tax Income</b>
              </p>
              <div id="">
                <div class="row mt-3">
                  <div class="col-md-6">
                    <label for="">Personal Allowance(20% of Total Income Plus N200,000)</label>
                    <input class="form-control" type="text" name="personal_allowance" value="">
                  </div>
                </div>
                <div class="row mt-3">
                  <div class="col-md-6">
                    <label for="">National Housing Fund</label>
                    <input class="form-control" type="text" name="nhf" value="">
                  </div>
                </div>
                <div class="row mt-3">
                  <div class="col-md-6">
                    <label for="">National Health Insurance</label>
                    <input class="form-control" type="text" name="nhi" value="">
                  </div>
                </div>
                <div class="row mt-3">
                  <div class="col-md-6">
                    <label for="">Life Assurance</label>
                    <input class="form-control" type="text" name="life_assurance" value="">
                  </div>
                </div>
                <div class="row mt-3">
                  <div class="col-md-6">
                    <label for="">National Pension Scheme</label>
                    <input class="form-control" type="text" name="pension" value="">
                  </div>
                </div>
                <div class="row mt-3">
                  <div class="col-md-6">
                    <label for="">Dividend</label>
                    <input class="form-control" type="text" name="dividend" value="">
                  </div>
                </div>
                <div class="row mt-3">
                  <div class="col-md-6">
                    <label for="">Interest</label>
                    <input class="form-control" type="text" name="interest" value="">
                  </div>
                </div>
              </div>

              <div class="mt-5">
                <button type="submit" class="btn btn-success">Save Assessment</button>
              </div>
            </form>


          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ asset('assets/js/select2.js') }}"></script>
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
  <script>
    $( "#addExpenseItem" ).click(function() {
      $("#less_expenses_div").append('<div class="row mt-3"><div class="col-md-6"><input class="form-control" type="text" name="less_expenses_item[]" value=""></div><div class="col-md-6"><input class="form-control" type="text" name="less_expenses_amount[]" value=""></div></div>');
    });

    $( "#addEmployeeIncome" ).click(function() {
      $("#employee_income_div").append('<div class="row mt-3"><div class="col-md-6"><input class="form-control" type="text" name="employee_income_item[]" value=""></div><div class="col-md-6"><input class="form-control" type="text" name="employee_income_amount[]" value=""></div></div>');
    });

    $( "#addOtherIncome" ).click(function() {
      $("#other_income_div").append('<div class="row mt-3"><div class="col-md-6"><input class="form-control" type="text" name="other_income_item[]" value=""></div><div class="col-md-6"><input class="form-control" type="text" name="other_income_amount[]" value=""></div></div>');
    });
  </script>
@endsection
