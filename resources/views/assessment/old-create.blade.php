@extends('layouts.main')

@section('title')
Assessment
@endsection

@section('content')
  <div class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card px-2">
          <div class="card-body">
            <div class="container-fluid">
              <h3 class="text-right my-5">Invoice</h3>
              <hr>
            </div>
            <div class="container-fluid d-flex justify-content-between">
              <div class="col-lg-3 pl-0">
                <p class="mt-5 mb-2">
                  <b>Generate Invoices</b>
                </p>
                <p>Please choose what you want to pay for.</p>
              </div>
            </div>
            <div class="container-fluid ">
              <form class="forms-sample row">
                @csrf
                <div class="form-group col-4">
                  <label for="heading_id">Revenue Heading</label>
                  <select class="form-control" name="heading_id" id="heading_dropdown" {{count($headings) < 1 ? "disabled":""}}>
                    <option value="">choose Revenue Heading...</option>
                    @foreach($headings as $heading)
                    <option value="{{ $heading->id }}">{{ ucfirst($heading->name) }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group col-4">
                  <label for="sub_heading_id">Sub-Heading</label>
                  <select class="form-control" name="sub_heading_id" id="subheading_dropdown">
                    <option value="">choose Revenue Sub-Heading...</option>
                  </select>
                </div>
                <div class="form-group col-2">
                  <label>Cost (&#x20A6;)</label>
                  <input type="number" class="form-control" id="unit_price" placeholder="Amount">
                </div>
                <div class="form-group col-2">
                  </br>
                  <button type="button" class="btn btn-success mr-2" onclick="addItem()">Add</button>
                </div>
              </form>
            </div>
            <div class="container-fluid mt-5 d-flex justify-content-center w-100">
              <div class="table-responsive w-100">
                <table class="table">
                  <thead>
                    <tr class="bg-dark text-white">
                      <th>Heading</th>
                      <th>Sub-Heading</th>
                      <th class="text-right">Unit cost</th>
                      <th class="text-right">Action</th>
                    </tr>
                  </thead>
                  <tbody id="purchase_list">

                  </tbody>
                </table>
              </div>
            </div>
            <div class="container-fluid mt-5 w-100">
              <h4 class="text-right mb-5" id="totalAmount">Total : </h4>
              <hr>
            </div>
            <div class="clearfix"></div>
            {{Form::open(['route' => 'store_assessment', 'method' => 'POST'])}}
              {{ csrf_field() }}
              <input type="hidden" class="form-control" value="{{ $taxPayer->full_name }}" name="taxpayer">
              <input type="hidden" class="form-control" value="{{ $taxPayer->id }}" name="taxpayer_reference">
              <input type="hidden" class="form-control" id="completeAmount" name="amount">
              <input type="hidden" class="form-control" id="completeItem" name="details[]">
              <!-- <div class="text-right">
                <button class="btn btn-danger" type="submit"> Save </button>
                <button class="btn btn-success" type="submit"> Pay </button>
              </div> -->
              <div class="container-fluid w-100">
                <button type="submit" class="btn btn-success float-right mt-4">Save</button>

                <!-- <a href="#" class="btn btn-primary float-right mt-4 ml-2">
                  <i class="mdi mdi-printer mr-1"></i>Print</a>
                <a href="#" class="btn btn-success float-right mt-4">
                  <i class="mdi mdi-telegram mr-1"></i>Send Invoice</a> -->
              </div>
            {{Form::close()}}

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ asset('assets/js/select2.js') }}"></script>
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
  <script>
    $(function() {
      var updatesubHeadingDropdown = function() {
      if ($('#heading_dropdown').val() == '') {
        $('#subheading_dropdown').prop('disabled', true);
        $('#unit_price').attr('readonly', true);
      } else {
        $('#subheading_dropdown').prop('disabled', false);
      }
    };

    updatesubHeadingDropdown();

    $('#heading_dropdown').change(function() {
      updatesubHeadingDropdown();
      $('#unit_price').attr('readonly', true);
      // alert($('#heading_dropdown').val());
      $.ajax({
        "type":"GET",
        "url": "/ajax/assessment",
        "data": {
          "id": $('#heading_dropdown').val()
        },
        success: function(data) {
          $('#subheading_dropdown').empty();
          $("#subheading_dropdown").prepend("<option value='' selected='selected'>Sub-Heading</option>");
          $.each(data, function(i, subheading) {
            $('#subheading_dropdown').append($("<option>").text(subheading['name']).attr('value', subheading['id']));
          });
          updatesubHeadingDropdown();
        },
        error: function(xhr, ajaxOptions, thrownError) {
          console.log(xhr.responseText);
        }
      });
      });
    });
  </script>
  <script>
    $('#subheading_dropdown').change(function() {
      console.log("ok");
      $.ajax({
        'type':'GET',
        'url':'/ajax/unit-price',
        'data':{
          'id':$('#subheading_dropdown').val()
        },
        success: function(data) {
          if(data.pricing == 'not-fixed') {
            $('#unit_price').attr('readonly', false);
          }else{
            $('#unit_price').attr('readonly', true);
          }
          // $('#unit_price').text(Number(data.amount).toLocaleString()).attr('value', data.amount);
          $('#unit_price').val(data.amount);
        },
        error: function(xhr, ajaxOptions, thrownError) {
          console.log(xhr.responseText);
        }
      });
    });
  </script>
  <script>
    var assessmentList = [ ];

    //Add selected items to the array of items that need to be paid for.
    function addItem(){
      var heading = document.getElementById('heading_dropdown');
      var headingName = heading.options[heading.selectedIndex].text;
      var subHeading = document.getElementById('subheading_dropdown');
      var subHeadingName = subHeading.options[subHeading.selectedIndex].text;
      var amount = document.getElementById('unit_price').value;

      var assessmentArray = {
        heading: headingName,
        subheading: subHeadingName,
        amount: Number(amount)
      }
      console.log(assessmentArray, assessmentList);

      jQuery('#purchase_list').html('');
      assessmentList.push(assessmentArray);
      assessmentList.forEach(function(newData) {
        var index = assessmentList.indexOf(newData);
        document.getElementById('purchase_list').innerHTML += itemHtmlFromObject(newData, index);
        document.getElementById('completeItem').value = JSON.stringify(assessmentList);
      });

      document.getElementById('heading_dropdown').value = '';
      document.getElementById('subheading_dropdown').value = '';
      document.getElementById('unit_price').value = '';
      // $('#unit_price').prop('readonly', true);
    }

    //Return selected items in the array to a html table
    function itemHtmlFromObject(item, index) {
      let total = 0;
      assessmentList.forEach(function(result){
        console.log(Number(result['amount']));
        total += Number(result['amount']);
      });
      console.log(total);
      jQuery('#totalAmount').html('<b>Total : &#x20A6;' + total.toLocaleString() + '</b>');
      document.getElementById('completeAmount').value = total;
      console.log(document.getElementById('completeAmount').value,total);
      var html = '';
      html += '<tr>';
      html += '<td>' + item.heading + '</td>';
      html += '<td>' + item.subheading + '</td>';
      html += '<td class="text-right">&#x20A6;' + item.amount.toLocaleString() + '</td>';
      html += '<td style="color: red; float:right;"><button type="button" class="btn waves-effect waves-light btn-sm btn-danger" onclick ="removeFromArray(' + index + ')">X Remove</button></td>';
      html += '</tr>';
      return html;
    }

    //Remove item not needed from the array of items to be paid for
    function removeFromArray(itemIndex) {
      assessmentList.splice(itemIndex, 1);
      jQuery('#purchase_list').html('');
      assessmentList.forEach(function(newData) {
        var index = assessmentList.indexOf(newData);
        document.getElementById('purchase_list').innerHTML += itemHtmlFromObject(newData, index);
        document.getElementById('completeItem').value = assessmentList;
        // console.log(document.getElementById('completeItem').value);
      });
    }
  </script>
@endsection
