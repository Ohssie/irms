@extends('layouts.main')

@section('title')
{{-- Invoice - [{{ $invoice->assessment_reference }}] --}}
@endsection

@section('content')
  <div class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card px-2">
          <div class="card-body">
            <div class="container-fluid">
              <h3 class="text-right my-5">Assessment&nbsp;&nbsp;#{{ $assessment->assessment_year }}</h3>
              <hr>
            </div>
            <div class="container-fluid d-flex justify-content-between">
              <div class="col-lg-4 pl-0">
                <p class="mb-0 mt-5">Status : {{ $assessment->status}}</p>
              </div>
            </div>
            <div class="container-fluid mt-5 d-flex justify-content-center w-100">
              <div class="table-responsive w-100">
                <table class="table">
                  <thead>
                    <tr class="bg-dark text-white">
                      {{-- <th>#</th> --}}
                      <th>Item</th>
                      <th class="text-right">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach (json_decode($assessment->less_expenses) as $key => $expense)
                      <tr class="text-right">
                          <td class="text-left">{{$key}}</td>
                          <td class="text-left">{{ $expense }}</td>
                      </tr>
                    @endforeach

                    @foreach (json_decode($assessment->employee_income) as $key => $income)
                      <tr class="text-right">
                          <td class="text-left">{{$key}}</td>
                          <td class="text-left">{{ $income }}</td>
                      </tr>
                    @endforeach
                    @foreach (json_decode($assessment->other_incomes) as $key => $income)
                      <tr class="text-right">
                          <td class="text-left">{{$key}}</td>
                          <td class="text-left">{{ $income }}</td>
                      </tr>
                    @endforeach

                    @foreach (json_decode($assessment->relief_and_non_taxable_income) as $key => $relief)
                      <tr class="text-right">
                          <td class="text-left">{{$key}}</td>
                          <td class="text-left">{{ $relief }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>

        </div>
      </div>
    </div>
  </div>
@endsection
