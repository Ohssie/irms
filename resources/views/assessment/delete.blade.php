<div class="modal fade" id="delete_assessment_{{$ass->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edit GroupLabel">Delete Generated Invoice?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="s-text">Remove <strong class="modal-title">'{{ $ass->assessment_reference }}'</strong> from your list of Invoices? </br><span class="p-text">This cannot be undone.</span></p>
      </div>
      <div class="modal-footer">
        <form method="get" action="{{route('delete_assessment', \Crypt::encryptString($ass->id))}}">
        <button type="submit" id="submit_form" class="btn btn-danger mr-2">Delete</button>
        </form>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>