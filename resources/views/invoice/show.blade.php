@extends('layouts.main')

@section('title')
Invoice - [{{ $invoice->assessment_reference }}]
@endsection

@section('content')
  <div class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card px-2">
          <div class="card-body">
            <div class="container-fluid">
              <h3 class="text-right my-5">Invoice&nbsp;&nbsp;#{{ $invoice->assessment_reference }}</h3>
              <hr>
            </div>
            <div class="container-fluid d-flex justify-content-between">
              <div class="col-lg-3 pl-0">
                <p class="mt-5 mb-2">
                 
                </p>
                
              </div>
              <div class="col-lg-3 pr-0">
                <p class="mt-5 mb-2 text-right">
                  <b>Invoice to</b>
                </p>
                <p class="text-right">{{ $taxPayer->full_name }},
                  <br> {{ $taxPayer->email }}, {{ $taxPayer->phone_number }},
                  <br> {{ $taxPayer->address }}, {{ $taxPayer->lga }}.</p>
              </div>
            </div>
            <div class="container-fluid d-flex justify-content-between">
              <div class="col-lg-4 pl-0">
                <p class="mb-0 mt-5">Invoice Date : {{ date("jS F, Y", strtotime($invoice->created_at)) }}</p>
              </div>
            </div>
            <div class="container-fluid mt-5 d-flex justify-content-center w-100">
              <div class="table-responsive w-100">
                <table class="table">
                  <thead>
                    <tr class="bg-dark text-white">
                      <th>#</th>
                      <th>Heading</th>
                      <th class="text-right">Subheading</th>
                      <th class="text-right">Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach(json_decode($invoice->details, true) as $detail)
                    <tr class="text-right">
                      <td class="text-left">{{ $num++ }}</td>
                      <td class="text-left">{{ $detail['heading'] }}</td>
                      <td>{{ $detail['subheading'] }}</td>
                      <td>&#x20A6;{{ number_format($detail['amount'], 2) }}</td>
                    </tr>
                   @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <div class="container-fluid mt-5 w-100">
              <h4 class="text-right mb-5">Total : &#x20A6; {{  number_format($invoice->amount, 2) }}</h4>
              <hr>
            </div>
            <div class="container-fluid w-100">
              <td class="text-nowrap">
                <form method="POST" action="{{ route('paystack_pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                  <input type="hidden" name="email" value="ugendu04@gmail.com"> {{-- required --}}
                  <input type="hidden" name="amount" value="{{ $invoice->amount * 100}}"> {{-- required in kobo --}}
                  <input type="hidden" name="metadata" value="{{ json_encode($array = ['assessment_details' => $invoice->details, 'assessment_reference' => $invoice->assessment_reference, 'taxpayer' => $invoice->taxpayer_id,]) }}" > {{-- For other necessary things you want to add to your payload. it is optional though --}}
                  <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                  <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
                  {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}

                  <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}}
              <a href="#">
                <button class="btn btn-primary float-right mt-4 ml-2" type="submit"><i class="mdi mdi-printer mr-1"></i>Pay</button>
              </a>
                </form>
              <!-- <a href="#" class="btn btn-success float-right mt-4">
                <i class="mdi mdi-telegram mr-1"></i>Send Invoice</a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection