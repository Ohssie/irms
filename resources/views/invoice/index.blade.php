@extends('layouts.main')

@section('title')
Invoices
@endsection

@section('content')
  <div class="content-wrapper">
    <div class="row mb-4">
      <div class="col-12">
        @include('layouts.sessions')
      </div>
      <div class="col-12 d-flex align-items-center justify-content-between">
        <h4 class="page-title">Invoices</h4>
        <div class="d-flex align-items-center">
        @if(Sentinel::getUser()->inRole('tax-payer'))
          <div class="wrapper mr-4 d-none d-sm-block">
            <p class="mb-0">Generate a new
              <b class="mb-0">Invoice</b>
            </p>
          </div>
          <div class="wrapper">
            <a href="{{ route('create_invoice', \Crypt::encryptString(Sentinel::getUser()->id)) }}" class="btn btn-link btn-sm font-weight-bold">
              <button type="button" class="btn btn-outline-success btn-fw"><i class="icon-plus"></i> New</button>
            </a>
          </div>
        @endif
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Invoices</h4>
        <div class="row">
          <div class="col-12 table-responsive">
            <table id="order-listing" class="table">
              <thead>
                <tr>
                  <th>Reference</th>
                  <th>Taxpayer</th>
                  <th>Amount(&#x20A6;)</th>
                  <th>Details</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Reference</th>
                  <th>Taxpayer</th>
                  <th>Amount(&#x20A6;)</th>
                  <th>Details</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($invoices as $invoice)
                  <tr>
                    <td>{{ $invoice->reference }}</td>
                    @foreach($users as $user)
                    @if($user->id == $invoice->taxpayer_id)
                      <td>{{ ucwords($user->full_name) }}</td>
                    @endif
                    @endforeach
                    <td>&#x20A6;{{ number_format($invoice->amount, 2) }}</td>
                    <td>
                      @foreach(json_decode($invoice->details, true) as $item)
                        * {{ $item['subheading']}} </br>
                      @endforeach
                    </td>
                    <td>{{ date("jS F, Y", strtotime($invoice->created_at)) }}</td>
                    <!-- <td class="text-nowrap">
                      <form method="POST" action="{{ route('paystack_pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                        <input type="hidden" name="email" value="ugendu04@gmail.com"> {{-- required --}}
                        <input type="hidden" name="amount" value="{{ $invoice->amount * 100}}"> {{-- required in kobo --}}
                        <input type="hidden" name="metadata" value="{{ json_encode($array = ['assessment_details' => $invoice->details, 'assessment_reference' => $invoice->reference, 'taxpayer' => $invoice->taxpayer_reference,]) }}" > {{-- For other necessary things you want to add to your payload. it is optional though --}}
                        <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                        <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
                        {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}

                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}} -->

                          <!-- <button type="submit" class="btn btn-primary btn-sm" id="submit">Pay</button>
                        irms.com.ng-->
                        <td class="text-right">
                          <a href="{{ route('show_invoice', \Crypt::encryptString($invoice->id)) }}">
                            <button class="btn btn-light">
                              <i class="icon-eye text-primary"></i>View
                            </button>
                          </a>
                          <button class="btn btn-light" data-toggle="modal" data-target="#delete_invoice_{{$invoice->id}}">
                            <i class="icon-trash text-danger"></i>Remove
                          </button>
                        </td>
                      <!-- </form>
                    </td> -->
                  </tr>
                  @include('invoice.delete')
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ asset('assets/js/select2.js') }}"></script>
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
  <!-- <script src="../../js/data-table.js"></script>   -->
@endsection
