<?php

use Illuminate\Database\Seeder;
use Cartalyst\Sentinel\Roles\EloquentRole as Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::firstOrCreate(['slug' => 'super-admin'],
        [
          'id' => '1',
          'slug' => 'super-admin',
          'name' => 'Super Admin',
          'permissions' => [
            'add-manage-mda' => true,
            'add-manage-revenue-heading' => true,
            'add-manage-sub-revenue-heading' => true,
            'add-manage-sub-admin-users' => true,
            'add-manage-desk-officers-and-other-users' => true,
            'add-manage-tax-payers' => true,
            'generate-assessment-and-payment' => true,
            'edit-pending-assessment' => true,
            'view-assessment-table' => true,
            'view-transaction-table' => true,
            'reports' => true,
          ]
        ]);

        Role::firstOrCreate(['slug' => 'sub-admin'],
          [
          'id' => '2',
          'slug' => 'sub-admin',
          'name' => 'Sub Admin',
          'permissions' => [
            'add-manage-mda' => false,
            'add-manage-revenue-heading' => false,
            'add-manage-sub-revenue-heading' => false,
            'add-manage-sub-admin-users' => true,
            'add-manage-desk-officers-and-other-users' => true,
            'add-manage-tax-payers' => true,
            'generate-assessment-and-payment' => true,
            'edit-pending-assessment' => false,
            'view-assessment-table' => true,
            'view-transaction-table' => true,
            'reports' => true,
          ]
        ]);

        Role::firstOrCreate(['slug' => 'internal-revenue-staff'],
          [
          'id' => '3',
          'slug' => 'internal-revenue-staff',
          'name' => 'Internal Revenue Staff',
          'permissions' => [
            'add-manage-mda' => false,
            'add-manage-revenue-heading' => false,
            'add-manage-sub-revenue-heading' => false,
            'add-manage-sub-admin-users' => false,
            'add-manage-desk-officers-and-other-users' => false,
            'add-manage-tax-payers' => true,
            'generate-assessment-and-payment' => true,
            'edit-pending-assessment' => false,
            'view-assessment-table' => true,
            'view-transaction-table' => true,
            'reports' => true,
          ]
        ]);

        Role::firstOrCreate(['slug' => 'government-personel'],
          [
          'id' => '4',
          'slug' => 'government-personel',
          'name' => 'Government Personel',
          'permissions' => [
            'add-manage-mda' => false,
            'add-manage-revenue-heading' => false,
            'add-manage-sub-revenue-heading' => false,
            'add-manage-sub-admin-users' => false,
            'add-manage-desk-officers-and-other-users' => false,
            'add-manage-tax-payers' => false,
            'generate-assessment-and-payment' => false,
            'edit-pending-assessment' => false,
            'view-assessment-table' => false,
            'view-transaction-table' => true,
            'reports' => true,
          ]
        ]);

        Role::firstOrCreate(['slug' => 'accounts'],
          [
          'id' => '5',
          'slug' => 'accounts',
          'name' => 'Accounts',
          'permissions' => [
            'add-manage-mda' => false,
            'add-manage-revenue-heading' => false,
            'add-manage-sub-revenue-heading' => false,
            'add-manage-sub-admin-users' => false,
            'add-manage-desk-officers-and-other-users' => false,
            'add-manage-tax-payers' => false,
            'generate-assessment-and-payment' => false,
            'edit-pending-assessment' => false,
            'view-assessment-table' => false,
            'view-transaction-table' => true,
            'reports' => true,
          ]
        ]);

        Role::firstOrCreate(['slug' => 'desk-officer'],
          [
          'id' => '6',
          'slug' => 'desk-officer',
          'name' => 'Desk Officer',
          'permissions' => [
            'add-manage-mda' => false,
            'add-manage-revenue-heading' => false,
            'add-manage-sub-revenue-heading' => false,
            'add-manage-sub-admin-users' => false,
            'add-manage-desk-officers-and-other-users' => false,
            'add-manage-tax-payers' => true,
            'generate-assessment-and-payment' => true,
            'edit-pending-assessment' => false,
            'view-assessment-table' => true,
            'view-transaction-table' => true,
            'reports' => true,
          ]
        ]);

        Role::firstOrCreate(['slug' => 'tax-payer'],
          [
          'id' => '7',
          'slug' => 'tax-payer',
          'name' => 'Tax Payer',
          'permissions' => [
            'generate-assessment-and-payment' => true,
            'edit-pending-assessment' => false,
            'view-assessment-table' => true,
            'view-transaction-table' => true,
            'reports' => true,
          ]
        ]);
    }
}
