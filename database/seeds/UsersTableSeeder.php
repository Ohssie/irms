<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdminDetails = [
          'full_name' => 'John Doe',
          'phone_number' => '1234567890',
          'email' => 'super@irms.com',
          'password' => 'secret',
        ];

        $super_admin = Sentinel::registerAndActivate($superAdminDetails, true);
        $role = Sentinel::findRoleBySlug('super-admin');
        $role->users()->attach($super_admin);

        $adminDetails = [
            'full_name' => 'Susy Lee',
            'phone_number' => '1234567890',
            'email' => 'admin@irms.com',
            'password' => 'secret',
        ];

        $admin = Sentinel::registerAndActivate($adminDetails, true);
        $role = Sentinel::findRoleBySlug('sub-admin');
        $role->users()->attach($admin);

        $dataEntryUser = [
            'full_name' => 'Jane Decker',
            'phone_number' => '1234567890',
            'email' => 'data@irms.com',
            'password' => 'secret',
        ];

        $user = Sentinel::registerAndActivate($dataEntryUser, true);
        $role = Sentinel::findRoleBySlug('desk-officer');
        $role->users()->attach($user);

        $taxpayer = [
            'full_name' => 'Quincy Larson',
            'phone_number' => '1234567890',
            'email' => 'taxpayer@irms.com',
            'password' => 'secret',
        ];

        $user = Sentinel::registerAndActivate($taxpayer, true);
        $role = Sentinel::findRoleBySlug('tax-payer');
        $role->users()->attach($user);
      }

}
