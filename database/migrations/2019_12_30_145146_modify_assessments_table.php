<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('assessments');

        Schema::create('assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('assessment_year');
            $table->text('postal_code')->nullable();
            $table->json('less_expenses');
            $table->json('employee_income');
            $table->json('other_incomes');
            $table->json('relief_and_non_taxable_income');
            $table->string('status');
            $table->unsignedInteger('taxpayer_id');
            $table->foreign('taxpayer_id')->references('id')->on('users');
            $table->unsignedInteger('generated_by')->nullable();
            $table->foreign('generated_by')->references('id')->on('users');
            $table->timestamp('date_verified')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessments');
        Schema::create('assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('assessment_reference');
            $table->string('status')->default('pending');
            $table->json('details');
            $table->float('amount')->default(0.00);
            $table->enum('payment_flag', ['awaiting', 'paid'])->default('awaiting');
            $table->unsignedInteger('taxpayer_id');
            $table->foreign('taxpayer_id')->references('id')->on('users');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }
}
