<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubHeadingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_headings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->enum('pricing', ['fixed', 'not-fixed']);
            $table->decimal('amount', 8, 2)->default(0.00)->nullable();
            $table->unsignedInteger('heading_id');
            $table->foreign('heading_id')->references('id')->on('headings');
            $table->enum('recurrent', ['yes', 'no']);
            $table->integer('validity')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_headings');
    }
}
