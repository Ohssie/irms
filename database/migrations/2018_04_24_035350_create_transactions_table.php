<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tranx_id');
            $table->string('tranx_ref');
            $table->string('gateway_response');
            $table->string('authorization_code');
            $table->integer('amount'); //In kobo
            $table->string('taxpayer');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            // $table->unsignedInteger('assessment_id');
            // $table->foreign('assessment_id')->references('id')->on('assessments')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('transactions');
    }
}
