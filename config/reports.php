<?php

return [

    'categories' => [
        'tax-payer' => [
          'name' => 'Tax Payers',
          'value' => 'taxpayers'
        ],
        'heading' => [
          'name' => 'Headings',
          'value' => 'headings'
        ],
        'sub-heading' => [
          'name' => 'Sub Headings',
          'value' => 'sub-heading'
        ],
    ],
    'filters' => [
      'month' => [
        'name' => 'By Month',
        'value' => 'month',
      ],
      'day' => [
        'name' => 'By Day',
        'value' => 'day',
      ]
    ],
    'years' => [
      '2018',
      '2019',
      '2020',
      '2021',
      '2022',
      '2023'
    ],
    'months' => [
      1 => [
        'name' =>  'January',
        'limit' => 31,
      ],
      2 => [
        'name' =>  'February',
        'limit' => 28,
      ],
      3 => [
        'name' =>  'March',
        'limit' => 31,
      ],
      4 => [
        'name' =>  'April',
        'limit' => 30,
      ],
      5 => [
        'name' =>  'May',
        'limit' => 31,
      ],
      6 => [
        'name' =>  'June',
        'limit' => 30,
      ],
      7 => [
        'name' =>  'July',
        'limit' => 31,
      ],
      8 => [
        'name' =>  'August',
        'limit' => 31,
      ],
      9 => [
        'name' =>  'September',
        'limit' => 30,
      ],
      10 => [
        'name' =>  'October',
        'limit' => 31,
      ],
      11 => [
        'name' =>  'November',
        'limit' => 30,
      ],
      12 => [
        'name' =>  'December',
        'limit' => 31,
      ],
    ],
];
