<?php

return [

    'kogi_lgas' => [
      "Adavi",
      "Ajaokuta",
      "Ankpa",
      "Bassa",
      "Dekina",
      "Ibaji",
      "Idah",
      "Igalamela-Odolu",
      "Ijumu",
      "Kabba/Bunu",
      "Koton Karfe",
      "Lokoja",
      "Mopa-Muro",
      "Ofu",
      "Ogori/Magongo",
      "Okehi",
      "Okene",
      "Olamaboro",
      "Omala",
      "Yagba East",
      "Yagba West",
    ],

    'occupations' => [
      'Lawyer',
      'Doctor',
      'Pastor',
      'Teacher',
      'Engineer',
      'Programmer',
      'Politician',
      'Business Man'
    ]
];
