<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
  Use softDeletes;

    public $table = 'transactions';

    protected $dates = ['deleted_at'];
}
