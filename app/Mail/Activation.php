<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Illuminate\Support\Facades\URL;

class Activation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;
    public $key;
    public function __construct($user, $code)
    {
      $this->user = $user;
      $this->key = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.activation')->subject("Confirm your account, ".$this->user->full_name)
                     ->with('url', URL::to('activation/'.$this->user->id.'/'.$this->key->code));
    }
}
