<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;
class AssessmentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Assessment\AssessmentContract',
            'App\Repositories\Assessment\EloquentAssessmentRepository');
    }
}
