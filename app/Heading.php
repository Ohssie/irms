<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Heading extends Model
{
    public function subHeading() {
        return $this->hasMany('App\SubHeadng');
    }

    public function users()
    {
      return $this->belongsTo('App\User');
    }

    public function mda()
    {
      return $this->belongsTo('App\Mda');
    }
}
