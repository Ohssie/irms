<?php

namespace App\Http\Middleware;

use Closure;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!\Sentinel::getUser()) {
                return redirect()->route('get_login')
                    ->with('error', 'Session expired. Login again!');
            }
        } catch (\BadMethodCallException $e) {
            return redirect()->route('get_login')
                    ->with('error', 'Session expired. Login again!');
        }
        return $next($request);
    }
}
