<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TaxPayerGroup\TaxPayerGroupContract;

class TaxPayerGroupController extends Controller
{
    protected $repo;
    public function __construct(TaxPayerGroupContract $tax_payer_group_contract)
    {
      $this->repo = $tax_payer_group_contract;
    }

    public function index()
    {
      $tax_payer_groups = $this->repo->findAll();
      return view('tax-payer-group.index', compact('tax_payer_groups'));
    }

    public function create(Request $request)
    {
      $tax_payer_group = $this->repo->create($request);
      if($tax_payer_group){
        return back()->with('success', 'Group created successfully');
      }
    }

    public function update(Request $request, $id)
    {
      $tax_payer_group = $this->repo->edit($request, $id);
      if($tax_payer_group){
        return back()->with('success', 'Group updated successfully');
      }
    }

    public function deleteTaxPayerGroup($id)
    {
      $tax_payer_group = $this->repo->discard($id);
      if($tax_payer_group){
        return back()->with('success', 'Group deleted successfully');
      }
    }
}
