<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Auth\AuthContract;
use App\Repositories\TaxPayerGroup\TaxPayerGroupContract;
use App\Repositories\TaxPayer\TaxPayerContract;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\User;
use Reminder;
use App\Mail\PasswordReset;


class AuthController extends Controller
{
    protected $repo;
    protected $taxPayerGroup;
    protected $taxPayer;

    public function __construct(AuthContract $authContract, TaxPayerGroupContract $taxPayerGroupContract,
                                TaxPayerContract $taxPayerContract) {
        $this->repo = $authContract;
        $this->taxPayerGroup = $taxPayerGroupContract;
        $this->taxPayer = $taxPayerContract;
    }

    public function signupPage(){
        $groups = $this->taxPayerGroup->findAll();
        return view('auth.signup')->with('groups', $groups);
    }

    public function handleSignUp(Request $request)
    {
        $this->validate($request, [
            'email' => 'unique:users',
            'full_name' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'occupation' => 'required',
            'lga' => 'required',
            'tax_payer_group_id' => 'required',
            'password' => 'required|confirmed',
        ]);

        try {
          $user = $this->repo->createTaxpayer($request);

          if($user && sendSms($user)){
            return redirect()->route('confirmation-page', \Crypt::encryptString($user->id))->with('success', 'Registration was Successful');
          }else{
            return redirect()->back()
                    ->withInput()
                    ->with('error', 'Registration failed. Please try again!');
          }
        } catch(\Exception $e) {
          return redirect()->back()->withInput()->with('error', $e->getMessage());
          // 'Issues encountered. Please try again.'
        }
    }

    public function login()
    {
      return view('auth.login');
    }

    public function handleLogin(Request $request)
    {
        $credentials = [
            'login' => $request->email,
            'password' => $request->password,
        ];
        try {
          $user = Sentinel::authenticate($credentials, true);
          if(!Sentinel::authenticate($credentials)) {
              return redirect()->back()->withInput()
              ->with('error', 'Your credentials did not match our records!');
          }
        } catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {
                return redirect()->back()->withInput()
                                 ->with('error', 'Your Account has not been activated. Check your mail to activate');
        }catch(\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e){
              $delay = $e->getDelay();
              return redirect()->back()->withInput()
                               ->with('error', 'You are banned for '.$delay. ' seconds.');
          }
              Sentinel::login($user);
            try {
                if (Sentinel::getUser()->inRole('super-admin')) {
                  return redirect()->route('user_dashboard');
                }
                else if (Sentinel::getUser()->inRole('sub-admin')) {
                  return redirect()->route('user_dashboard');
                }
                else if (Sentinel::getUser()->inRole('data-entry')) {
                  return redirect()->route('tax_payers');
                }
                else if (Sentinel::getUser()->inRole('tax-payer')) {
                    if($user->is_tax_payer_confirmed){
                      return redirect()->route('invoices');
                    }else{
                      return redirect()->route('confirmation-page', \Crypt::encryptString($user->id));
                    }
                  }


            } catch (BadMethodCallException $e) {
                return redirect()->route('home')
                        ->with('error', 'Your Session has expired. Please login again!');
            }
    }

    public function activateUserView($userId, $code){
      $user = Sentinel::findById($userId);
      $activation = \Activation::complete($user, $code);
      return redirect()->route('get_login');
    }

    public function logout()
    {
        try {
            $user = Sentinel::getUser();
            Sentinel::logout($user, true);
            session()->flush();
            return redirect()->route('get_login');
        } catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {
            return redirect()->route('index_page');
        } catch (\ErrorException $e) {
            return redirect()->route('get_login')
                ->with('error', 'Session expired. Login again!');
        }
    }

    public function forgotPassword()
    {
      return view('auth.forgot-password');
    }

    public function sendResetPasswordEmail(Request $request) {
      $this->validate($request, [
        'email' => 'required'
        ]);
        $user = User::where('email', $request->email)->first();
        if(!$user){
          return response()->json([
              'code' => '-100',
              'msg' => 'This email does not exist on our database!!!'
          ]);
          // return "This email does not exist on our database!!!";
        }else{
          if(!$reminder = Reminder::exists($user)){
              $reminder = Reminder::create($user);
          }
          \Mail::to($user)->send(new PasswordReset($user, $reminder->code));

          return response()->json([
              'code' => '100',
              'msg' => 'A link has been sent to you, check your email!!!'
          ]);
        }
    }

    public function recover($id, $code)
    {
      return view('auth.recover-password')->with('id', $id)->with('code', $code);
    }

    public function newPassword(Request $request){
      $this->validate($request, [
            'password' => 'required',
            'confirm_password' => 'required',
        ]);
        if(($request->password) != ($request->confirm_password)){
          return back()
                  ->with('error', 'Passwords do not match. Try Again!!!');
        }
        $user = Sentinel::findById($request->id);
        if ($reminder = Reminder::complete($user, $request->code, $request->confirm_password)){
          Sentinel::login($user);
          return redirect()->route('user_profile', $user->slug)
                  ->with('success', 'Password updated successfully');
        }else{
          return back()
                  ->with('error', 'Password Reset Error, Try Again!!!');
        }
        $user->password =  bcrypt($request->password);
        $user->save();

    }

    public function dashboard()
    {
      $user = Sentinel::getUser();
      return view('layouts.index', compact('user'));
    }

    public function confirmationPage($id)
    {
      $user = $this->repo->findUser(\Crypt::decryptString($id));
      return view('auth.confirmation')->with('user', $user);
    }

    public function confirmUser(Request $request)
    {
      $confirm_user = $this->repo->confirmUser($request);
      if($confirm_user == true)
      {
        Sentinel::login($this->repo->findUser($request->user_id));
        return redirect()->route('invoices')->with('success', 'Your account confirmation was successful!');
      }else{
        return back()->with('error', 'Your confirmation code is not correct.');
      }
    }

    public function resendConfirmationMessage($user_id)
    {
      $user = $this->repo->findUser(\Crypt::decryptString($user_id));
      $sms_status = sendSms($user);
      // dd($sms_status);

      return back()->with('success', 'Verification code sent successfully, Please check your phone');
    }
}
