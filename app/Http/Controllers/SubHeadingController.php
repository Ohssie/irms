<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SubHeading\SubHeadingContract;
use App\Repositories\Heading\HeadingContract;

class SubHeadingController extends Controller
{
    protected $repo;
    protected $headingRepo;

    public function __construct(SubHeadingContract $subHeadingContract, HeadingContract $headingContract) {
        $this->repo = $subHeadingContract;
        $this->headingRepo = $headingContract;
    }

    public function index()
    {
        $subs = $this->repo->findAll();
        $headings = $this->headingRepo->findAll();
        return view('subheading.index')->with('subs', $subs)->with('headings', $headings);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:sub_headings',
            'code' => 'required|unique:sub_headings',
        ]);

        try {
            $sub = $this->repo->create($request);
            if ($sub) {
                return redirect()->route('sub_headings')->with('success', 'Revenue sub-heading created successfully!');
            } else {
                return back()->withInput()->with('error', 'Something went wrong, try again');
            }
        } catch (\Exception $e) {
            return back()->withInput()->with('error', 'Something went wrong, try again');
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required',
        ]);

        // try {
            $sub = $this->repo->edit($id, $request);
            if ($sub) {
                return redirect()->route('sub_headings')->with('success', 'Revenue sub-heading updated successfully!');
            } else {
                return back()->withInput()->with('error', 'Could not update sub-heading. Try again!');
            }
        // } catch (\Exception $e) {
        //     return back()->withInput()->with('error', 'Issues encountered, please try again!');
        // }
    }

    public function findSubHeadings(Request $request) {
        return $this->repo->findByHeading($request->id);
    }

    public function getPrice(Request $request) {
        return $this->repo->findPrice($request->id);
    }

    public function delete($id)
    {
        try {
            $sub = $this->repo->remove($id);
            if($sub) {
                return redirect()->route('sub_headings')->with('success', 'Successfully deleted revenue sub-heading!');
            } else {
                return back()->with('error', 'Sub-heading could not be deleted, try again!');
            }
        } catch(\Exception $e) {
            return back()->with('error', 'Issues encountered, please try again!');
        }
    }
}
