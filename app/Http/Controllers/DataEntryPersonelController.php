<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\User;
use App\Repositories\Auth\AuthContract;
use App\Http\Requests\UserRequest;

class DataEntryPersonelController extends Controller
{
    protected $repo;
    public function __construct(AuthContract $auth_contract)
    {
      $this->repo = $auth_contract;
    }

    public function index()
    {
      $role = Sentinel::findRoleBySlug('data-entry');
      $users = $role->users()->with('roles')->get();
      return view('admin.manage_users', compact('users'));
    }

    public function create(Request $request)
    {
      $data_entry_personel = $this->repo->createDataEntryPersonel($request);
      return redirect()->route('manage_users')->with(notification('success', 'Data Entry Personel has been created successfully!'));
    }

    public function update(Request $request, $id)
    {
      $data_entry_personel = $this->repo->updateDataEntryPersonel($request, $id);
      return redirect()->route('manage_users')->with(notification('success', 'Data Entry Personel has been updated successfully!'));
    }

    public function deleteDataEntryPersonel($id)
    {
      $data_entry_personel = $this->repo->deleteDataEntryPersonel($id);
      if($data_entry_personel){
        return redirect()->route('manage_users')->with(notification('success', 'Data Entry Personel has been deleted successfully!'));
      }
    }
}
