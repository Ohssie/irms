<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Invoice\InvoiceContract;
use App\Repositories\Heading\HeadingContract;
use App\Repositories\SubHeading\SubHeadingContract;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Repositories\User\UserContract;
use App\Repositories\TaxPayerGroup\TaxPayerGroupContract;
use App\Repositories\TaxPayer\TaxPayerContract;

class InvoiceController extends Controller
{
    protected $invoice;
    protected $heading;
    protected $subHeading;
    protected $userRepo;
    protected $taxPayerGroup;
    protected $taxPayer;

    public function __construct(InvoiceContract $invoiceContract, HeadingContract $headingContract,
                                SubHeadingContract $subHeadingContract, UserContract $userContract,
                                TaxPayerContract $taxPayerContract, TaxPayerGroupContract $taxPayerGroupContract)
    {
        $this->invoice = $invoiceContract;
        $this->heading = $headingContract;
        $this->subHeading = $subHeadingContract;
        $this->userRepo = $userContract;
        $this->taxPayerGroup = $taxPayerGroupContract;
        $this->taxPayer = $taxPayerContract;
    }

    public function index()
    {
        $users = $this->userRepo->findAllUsers();
        if(Sentinel::getUser()->inRole('tax-payer')) {
            $user = Sentinel::getUser();
            $invoices = $this->invoice->individualInvoices($user->id);
        } else  {
            $invoices = $this->invoice->findAll();
        }
        return view('invoice.index')->with('invoices', $invoices)->with('users', $users);
    }

    public function create($id)
    {
        $id = \Crypt::decryptString($id);
        $taxPayer = $this->taxPayer->findById($id);
        $headings = $this->heading->findAll();
        if(Sentinel::getUser()->inRole('tax-payer')) {
            $subHeadings = $this->subHeading->allFixedSubheadings();
        } else {
            $subHeadings = $this->subHeading->findAll();
        }
        return view('invoice.create')->with('headings', $headings)->with('subHeadings', $subHeadings)
                                        ->with('taxPayer', $taxPayer);
    }

    public function show($id)
    {
        $id = \Crypt::decryptString($id);
        $invoice = $this->invoice->findById($id);
        $taxPayer = $this->taxPayer->findById($invoice->taxpayer_id);
        $num = 1;
        return view('invoice.show')->with('invoice', $invoice)->with('taxPayer', $taxPayer)->with('num', $num);
    }

    public function store(Request $request)
    {
        try {
            $invoice = $this->invoice->create($request);
            if ($invoice) {
                return redirect()->route('invoices')->with('success', 'Invoice created successfully!');
            } else {
                return back()->withInput()->with('error', 'Invoice generation failed, Try again!!');
            }
        } catch(\Exception $e) {
            return back()->withInput()->with('error', 'Invoice generation failed, Try again!');
        }
    }

    public function quickPayment()
    {
        $subHeadings = $this->subHeading->allFixedSubheadings();
        $groups = $this->taxPayerGroup->findAll();
        return view('quick_payment')->with('groups', $groups)->with('subHeadings', $subHeadings);
    }

    public function delete($id)
    {
        try {
            $invoice = $this->invoice->delete($id);
            if($invoice) {
                return redirect()->route('invoices')->with('success', 'Successfully deleted Invoice!');
            } else {
                return back()->with('error', 'Invoice could not be deleted, try again!');
            }
        } catch(\Exception $e) {
            return back()->with('error', 'Issues encountered, please try again!');
        }
    }
}
