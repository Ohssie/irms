<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Transaction\TransactionContract;

class DashboardController extends Controller
{
    protected $transactionRepo;

    public function __construct(TransactionContract $transactionContract) {
        $this->transactionRepo = $transactionContract;
    }

    public function index() {
        //Remittance History
        $today = $this->transactionRepo->todaysRemittance();
        $thisMonth = $this->transactionRepo->thisMonthsRemittance();
        $lastMonth = $this->transactionRepo->lastMonthsRemittance();
        $sinceInception = $this->transactionRepo->totalFromInception();
        return view('dashboard')->with('sinceInception', $sinceInception)->with('thisMonth', $thisMonth)
                                ->with('today', $today)->with('lastMonth', $lastMonth);
    }
}
