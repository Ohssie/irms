<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\Repositories\User\UserContract;
use App\Repositories\Mda\MdaContract;
use App\Http\Requests\UserRequest;
use App\Repositories\TaxPayer\TaxPayerContract;

class UserController extends Controller
{
  protected $repo;
  protected $groupRepo;
  
  public function __construct(UserContract $user_contract, MdaContract $mda_contract, TaxPayerContract $taxpayerGroupContract)
  {
    $this->repo = $user_contract;
    $this->mdaRepo = $mda_contract;
    $this->groupRepo = $taxpayerGroupContract;
  }

  public function index()
  {
    $users = $this->repo->findAll();
    $roles = $this->repo->findAllRoles();
    $mdas = $this->mdaRepo->findAll();
    return view('user.index', compact('users', 'roles', 'mdas'));
  }

  public function create(Request $request)
  {
    $data_entry_personel = $this->repo->createUser($request);
    return redirect()->route('manage_users')->with('success', 'User record has been created successfully!');
  }

  public function update(Request $request, $id)
  {
    $data_entry_personel = $this->repo->updateUser($request, $id);
    return redirect()->route('manage_users')->with('success', 'User record has been updated successfully!');
  }

  public function deleteDataEntryPersonel($id)
  {
    $data_entry_personel = $this->repo->deleteUser($id);
    if($data_entry_personel){
      return redirect()->route('manage_users')->with('success', 'User record has been deleted successfully!');
    }
  }

  public function allTaxpayers() {
    $roles = $this->repo->findAllRoles();
    $taxpayers = $this->repo->allTaxpayers();
    $mdas = $this->mdaRepo->findAll();
    $taxGroups = $this->groupRepo->findAll();
    return view('taxpayer.index', compact('roles', 'taxpayers', 'mdas', 'taxGroups'));
  }
}
