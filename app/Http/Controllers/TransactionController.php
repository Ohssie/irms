<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Transaction\TransactionContract;
use App\User;
use App\Repositories\TaxPayer\TaxPayerContract;

class TransactionController extends Controller
{
    protected $tranx;
    protected $taxPayer;

    public function __construct(TransactionContract $tranxContract, TaxPayerContract $taxPayerContract) {
        $this->tranx =  $tranxContract;
        $this->taxPayer = $taxPayerContract;
    }

    public function index() {
        $tranx = $this->tranx->findAll();
        $users = User::all();
        $taxPayers = $this->taxPayer->findAll();
        return view('transactions.index')->with('tranx', $tranx)->with('users', $users)->with('taxPayers', $taxPayers);
    }

    public function storeTransaction(Request $request) {
        dd($request);
    }
}
