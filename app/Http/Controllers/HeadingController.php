<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Heading\HeadingContract;
use App\Repositories\Mda\MdaContract;


class HeadingController extends Controller
{
    protected $repo;
    protected $mdaRepo;

    public function __construct(HeadingContract $headingContract, MdaContract $mdaContract) {
        $this->repo = $headingContract;
        $this->mdaRepo = $mdaContract;
    }

    public function index()
    {
        $headings = $this->repo->findAll();
        $mdas = $this->mdaRepo->findAll();
        return view('heading.index')->with('headings', $headings)->with('mdas', $mdas);
    }

    public function store(Request $request)
    {
      // dd($request);
        $this->validate($request, [
            'name' => 'required|unique:headings',
            'code' => 'required|unique:headings',
        ]);

        try {
            $heading = $this->repo->create($request);
            if ($heading) {
                return redirect()->route('headings')->with('success', 'Heading created successfully!');
            } else {
                return back()->withInput()->with('error', 'Something went wrong, pls try again!');
            }
        } catch (\Exception $e) {
            return back()->withInput()->with('error', 'Something went wrong!');
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required',
        ]);

        try {
            $heading = $this->repo->edit($id, $request);
            if ($heading) {
                return redirect()->route('headings')->with('success', 'Heading updated successfully!');
            } else {
                return back()->withInput()->with('error', 'Could not update Heading. Try again!');
            }
        } catch (\Exception $e) {
            return back()->withInput()->with('error', 'Issues encountered, please try again!');
        }
    }

    public function delete($id)
    {
        try {
            $heading = $this->repo->remove($id);
            if($heading) {
                return redirect()->route('headings')->with('success', 'Successfully deleted heading!');
            } else {
                return back()->with('error', 'Heading could not be deleted, try again!');
            }
        } catch(\Exception $e) {
            return back()->with('error', 'Issues encountered, please try again!');
        }
    }
}
