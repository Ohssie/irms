<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Paystack;
use App\Repositories\Transaction\TransactionContract;
use App\Repositories\Invoice\InvoiceContract;

class PaymentController extends Controller
{
    protected $tranxRepo;
    protected $invoiceRepo;

    public function __construct(TransactionContract $tranxContract, InvoiceContract $invoiceContract) {
        $this->tranxRepo = $tranxContract;
        $this->invoiceRepo = $invoiceContract;
    }
    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        try {
            $paymentDetails = Paystack::getPaymentData();
            // dd($paymentDetails['data']);
            if ($paymentDetails['data']['status'] == 'success') {
                $transaction = $this->tranxRepo->create($paymentDetails);
                if($transaction) {
                    $ref = $paymentDetails['data']['metadata']['assessment_reference'];
                    $this->invoiceRepo->findByRef($ref);
                    return redirect()->route('invoices')->with('success', 'Payment was successful!');
                }
            } else {
                return redirect()->route('invoices')->with('error', 'Payment failed. Try again!');
            }
        } catch(\Exception $e) {
            return redirect()->route('invoices')->with('error', 'Payment failed. Try again! Details (' . $e->getMessage() . ')');
        }
    }
}
