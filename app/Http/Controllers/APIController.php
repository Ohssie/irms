<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APIController extends Controller
{
    public function registerTaxPayer(Request $request)
    {
      $this->validate($request, [
          'email' => 'unique:users',
          'full_name' => 'required',
          'phone_number' => 'required',
          'address' => 'required',
          'occupation' => 'required',
          'lga' => 'required',
          'tax_payer_group_id' => 'required',
          'password' => 'required|confirmed',
      ]);

      $user = $this->repo->createTaxpayer($request);

      return response()->json([
        'status' => 201,
        'success' => true,
        'user' => $user,
        'msg' => 'Tax Payer registered successfully'
      ]);
    }
}
