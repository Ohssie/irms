<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Repositories\TaxPayer\TaxPayerContract;
use App\Repositories\TaxPayerGroup\TaxPayerGroupContract;
use App\Http\Requests\TaxPayerRequest;
use App\Repositories\User\UserContract;
use App\Repositories\Mda\MdaContract;

class TaxPayerController extends Controller
{
  protected $repo;
  protected $groupRepo;

  public function __construct(TaxPayerContract $taxPayerContract, TaxPayerGroupContract $taxPayerGroupContract, 
                              MdaContract $mda_contract, UserContract $userContract)
  {
    $this->repo = $taxPayerContract;
    $this->userRepo = $userContract;
    $this->groupRepo = $taxPayerGroupContract;
    $this->mdaRepo = $mda_contract;
  }
  public function index()
  {
    $roles = $this->userRepo->findAllRoles();
    $taxpayers = $this->repo->findAll();
    $mdas = $this->mdaRepo->findAll();
    $taxGroups = $this->groupRepo->findAll();
    return view('taxpayer.index', compact('roles', 'mdas', 'taxpayers', 'taxGroups'));
  }

  public function store(TaxPayerRequest $request)
  {
    $tax_payer = $this->repo->create($request);
    return redirect()->route('tax_payers')->with(notification('success', 'Tax Payer has been created successfully!'));
  }

  public function update(Request $request, $id)
  {
    $tax_payer = $this->repo->edit($request, $id);
    return redirect()->route('tax_payers')->with(notification('success', 'Tax Payer has been updated successfully!'));
  }

  public function delete($id)
  {
    $tax_payer = $this->repo->remove($id);
    if($tax_payer){
      return redirect()->route('tax_payers')->with(notification('success', 'Tax Payer has been created successfully!'));
    }
  }
}
