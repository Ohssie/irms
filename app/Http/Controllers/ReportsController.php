<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TaxPayer\TaxPayerContract;
use App\Repositories\Heading\HeadingContract;
use App\Repositories\SubHeading\SubHeadingContract;

class ReportsController extends Controller
{
    protected $tax_payer_repo;
    protected $heading_repo;
    protected $sub_heading_repo;

    public function __construct(TaxPayerContract $tax_payer_contract, HeadingContract $heading_contract, SubHeadingContract $sub_heading_contract)
    {
      $this->tax_payer_repo = $tax_payer_contract;
      $this->heading_repo = $heading_contract;
      $this->sub_heading_repo = $sub_heading_contract;
    }

    public function getReports()
    {
      return view('reports.query');
    }

    public function reports(Request $request)
    {
      if ($request->category == null) {
        return back()->with('error', 'You have to select a category');
      }
      if($request->category == config("reports.categories.tax-payer.value")){
        $tax_payers = $this->tax_payer_repo->taxPayerReport($request);
        return view('reports.results', compact('tax_payers'));
      }

      if($request->category == config("reports.categories.heading.value")){
        $headings = $this->heading_repo->headingReport($request);
        return view('reports.results', compact('headings'));
      }

      if($request->category == config("reports.categories.sub-heading.value")){
        $sub_headings = $this->sub_heading_repo->subHeadingReport($request);
        return view('reports.results', compact('sub_headings'));
      }
    }
}
