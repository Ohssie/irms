<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Assessment\AssessmentContract;
use Sentinel;

class AssessmentController extends Controller
{
    protected $assessment_repo;

    public function __construct(AssessmentContract $assessment_contract)
    {
      $this->assessment_repo = $assessment_contract;
    }

    public function index()
    {
      $assessments = $this->assessment_repo->getUserAssessments(Sentinel::getUser()->id);
      return view('assessment.index', compact('assessments'));
    }

    public function createAssessment()
    {
      return view('assessment.create');
    }

    public function saveAssessment(Request $request)
    {
      $assessment = $this->assessment_repo->saveAssessment($request);
      if($assessment) {
        return back()->with('success', 'Assessment saved successfully, and awaiting approval');
      }

    }

    public function viewAssessment($assessment_id)
    {
      $assessment = $this->assessment_repo->findAssessment($assessment_id);
      return view('assessment.show', compact('assessment'));
    }
}
