<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Mda\MdaContract;


class MdaController extends Controller
{
  protected $repo;

  public function __construct(MdaContract $mdaContract) {
      $this->repo = $mdaContract;
  }

  public function index()
  {
      $mdas = $this->repo->findAll();
      return view('mda.index')->with('mdas', $mdas);
  }

  public function store(Request $request)
  {
      $this->validate($request, [
          'name' => 'required|unique:mdas',
          'code' => 'required|unique:mdas',
      ]);

      try {
          $mda = $this->repo->create($request);
          if ($mda) {
              return redirect()->route('mdas')->with('success', 'MDA created successfully!');
          } else {
              return back()->withInput()->with('error', 'MDA ' . $request->name . ' exists!');
          }
      } catch (\Exception $e) {
          return back()->withInput()->with('error', 'MDA ' . $request->name . ' exists!');
      }
  }

  public function update(Request $request, $id)
  {
      $this->validate($request, [
          'name' => 'required',
          'code' => 'required',
      ]);

      try {
          $mda = $this->repo->edit($id, $request);
          if ($mda) {
              return redirect()->route('mdas')->with('success', 'MDA updated successfully!');
          } else {
              return back()->withInput()->with('error', 'Could not update MDA. Try again!');
          }
      } catch (\Exception $e) {
          return back()->withInput()->with('error', 'Issues encountered, please try again!');
      }
  }

  public function delete($id)
  {
      try {
          $mda = $this->repo->remove($id);
          if($mda) {
              return redirect()->route('mdas')->with('success', 'Successfully deleted MDA!');
          } else {
              return back()->with('error', 'MDA could not be deleted, try again!');
          }
      } catch(\Exception $e) {
          return back()->with('error', 'Issues encountered, please try again!');
      }
  }
}
