<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxPayerGroup extends Model
{
  Use softDeletes;

    public $table = 'tax_payer_groups';

    protected $dates = ['deleted_at'];
}
