<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubHeading extends Model
{
    public function heading() {
        return $this->belongsTo('App\Heading');
    }
}
