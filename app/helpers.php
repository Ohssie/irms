<?php
function notification($alert_type, $message)
{
  $notification = array('message' => $message, 'alert-type' => $alert_type );
  return $notification;
}

function saveUserAvatar($request)
{
  $destination = 'uploads/avatars';
  $extension = $request->file('avatar')->getClientOriginalExtension();
  $fileName = rand(1111111, 9999999).'.'.$extension;
  $request->file('avatar')->move($destination, $fileName);

  return ['destination' => $destination, 'file_name' => $fileName];
}

function uniqueIdentifier() {
  $id = date('YmdHis') . rand(100, 999);
  return $id;
}

function randomString($length) {
  $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  $encrpyt = substr( str_shuffle( $chars ), rand(0,70), $length );
  return $encrpyt;
}

function sendSms($user, $flash = 0)
{
    $url = $json_url = 'http://api.ebulksms.com:8080/sendsms.json';
    // my api key
    $apikey = '88a6c25a71566ad8e4566e95d1c87ecdbdf53e9f';
    // $apikey = 'b1b4172dd0628e619059417a714356f266ebde40';
    $flash=0;
    $sendername='iRMS';
    $gsm = array();
    $country_code = '234';
    $arr_recipient = explode(',', $user->phone_number);
    foreach ($arr_recipient as $recipient) {
        $mobilenumber = trim($recipient);
        if (substr($mobilenumber, 0, 1) == '0'){
            $mobilenumber = $country_code . substr($mobilenumber, 1);
        }
        elseif (substr($mobilenumber, 0, 1) == '+'){
            $mobilenumber = substr($mobilenumber, 1);
        }
        $generated_id = uniqid('int_', false);
        $generated_id = substr($generated_id, 0, 30);
        $gsm['gsm'][] = array('msidn' => $mobilenumber, 'msgid' => $generated_id);
    }
    $message = array(
        'sender' => $sendername,
        'messagetext' => "Hi ".$user->full_name.", Please verify your account with this code ". $user->confirmation_code,
        'flash' => "{$flash}",
    );

    $request = array('SMS' => array(
            'auth' => array(
                'username' => 'ugendu04@gmail.com',
                // 'username' => 'oejiga@opateq.com',
                'apikey' => $apikey
            ),
            'message' => $message,
            'recipients' => $gsm
    ));
    $json_data = json_encode($request);
    if ($json_data) {
        $response = doPostRequest($url, $json_data, array('Content-Type: application/json'));
        $result = json_decode($response);
        return $result->response->status;
    } else {
        return false;
    }
}


function doPostRequest($url, $data, $headers = array('Content-Type: application/x-www-form-urlencoded'))
{
     $php_errormsg = '';
     if (is_array($data)) {
         $data = http_build_query($data, '', '&');
     }
     $params = array('http' => array(
             'method' => 'POST',
             'content' => $data)
     );
     if ($headers !== null) {
         $params['http']['header'] = $headers;
     }
     $ctx = stream_context_create($params);
     $fp = fopen($url, 'rb', false, $ctx);
     if (!$fp) {
         return "Error: gateway is inaccessible";
     }
     //stream_set_timeout($fp, 0, 250);
     try {
         $response = stream_get_contents($fp);
         if ($response === false) {
             throw new Exception("Problem reading data from $url, $php_errormsg");
         }
         return $response;
     } catch (Exception $e) {
         $response = $e->getMessage();
         return $response;
     }
 }
