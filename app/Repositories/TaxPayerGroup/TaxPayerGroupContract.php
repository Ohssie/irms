<?php
namespace App\Repositories\TaxPayerGroup;

interface TaxPayerGroupContract
{
	public function create($request);
  public function edit($request, $id);
  public function findAll();
  public function findById($id);
  public function discard($id);
}
