<?php
namespace App\Repositories\TaxPayerGroup;

use App\Repositories\TaxPayerGroup\TaxPayerGroupContract;
use App\TaxPayerGroup;

class EloquentTaxPayerGroupRepository implements TaxPayerGroupContract
{
  public function create($request)
  {
    $tax_payer_group = new TaxPayerGroup;
    $tax_payer_group->shortcode = $request->shortcode;
    $tax_payer_group->name = $request->name;
    $tax_payer_group->save();
    return $tax_payer_group;
  }

  public function edit($request, $id)
  {
    $tax_payer_group = TaxPayerGroup::find($id);
    $tax_payer_group->shortcode = $request->shortcode;
    $tax_payer_group->name = $request->name;
    $tax_payer_group->save();
    return $tax_payer_group;
  }

  public function findAll()
  {
    return TaxPayerGroup::all();
  }

  public function findById($id)
  {
    return TaxPayerGroup::find($id);
  }

  public function discard($id)
  {
    $tax_payer_group = $this->findById($id);
    $tax_payer_group->delete();
    return $tax_payer_group;
  }
}
