<?php
namespace App\Repositories\Invoice;

interface InvoiceContract
{
	public function create($request);
	public function findByRef($ref);
	public function findById($id);
	public function individualInvoices($ref);
	public function findAll();
	public function delete($id);
}
