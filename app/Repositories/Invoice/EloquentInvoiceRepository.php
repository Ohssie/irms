<?php
namespace App\Repositories\Invoice;

use App\Invoice;
use Sentinel;
use Illuminate\Support\Facades\Crypt;

class EloquentInvoiceRepository implements InvoiceContract
{
	public function create($request)
  {
		$invoice = new Invoice();
		$this->invoicePropertiesSetup($invoice, $request);
		$invoice->save();
		return $invoice;
	}

	public function findById($id)
  {
		$invoice = Invoice::find($id);
		return $invoice;
	}

	public function individualInvoices($id) {
		$invoice = Invoice::where('taxpayer_id', $id)->where('status', 'pending')->get();
		return $invoice;
	}

	public function findByRef($ref)
  {
		$invoice = Invoice::where('invoice_reference', $ref)->first();
		$invoice->status = 'paid';
		$invoice->save();
		return $invoice;
	}

	public function findAll()
  {
		return Invoice::where('status', 'pending')->get();
	}

	public function delete($id)
  {
		$id = Crypt::decryptString($id);
		$invoice = $this->findById($id);
		return $invoice->delete();
	}

	private function invoicePropertiesSetup($invoice, $request)
  {
		$ref = randomString(10);
		$user = Sentinel::getUser();
		$details = $request->details;
		$details = implode(', ', $details);

		$invoice->reference = 'a_' . $ref;
		$invoice->taxpayer_id = $request->taxpayer_reference;
		$invoice->details = $details;
		$invoice->amount = intval($request->amount);
		$invoice->user_id = $user->id;
	}
}
