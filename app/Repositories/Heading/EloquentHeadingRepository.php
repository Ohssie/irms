<?php
namespace App\Repositories\Heading;

use App\Repositories\Heading\HeadingContract;
use App\Heading;

class EloquentHeadingRepository implements HeadingContract {

    public function create($request) {
        $heading = new Heading();
        $this->headingProperties($heading, $request);
        $heading->save();
        return $heading;
    }

    public function  edit($id, $request) {
        $heading = $this->findById($id);
        $this->headingProperties($heading, $request);
        $heading->save();
        return $heading;
    }

    public function findAll() {
        return Heading::all();
    }

    public function findById($id) {
        return Heading::find($id);
    }

    public function remove($id) {
        $heading = $this->findById($id);
        return $heading->delete();
    }

    private function headingProperties($heading, $request) {
        $heading->name = $request->name;
        $heading->code = $request->code;
        $heading->mda_id = $request->mda_id;
    }

    public function headingReport($request)
    {
      if($request->month){
        $month_vars = explode("|", $request->month);
        $num_padded = sprintf("%02d", $month_vars[2]);
        $headings = Heading::whereBetween('created_at', [$request->year.'-'.$num_padded.'-01', $request->year.'-'.$num_padded.'-'.$month_vars[1]])
                              ->orderBy('created_at', 'desc')->get();

      }elseif($request->day){
        $headings = Heading::whereDate('created_at', $request->day)->orderBy('created_at', 'desc')->get();
      }
      return $headings;
    }
}
