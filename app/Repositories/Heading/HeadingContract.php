<?php
namespace App\Repositories\Heading;

interface HeadingContract {
    public function create($request);
    public function edit($id, $request);
    public function findAll();
    public function findById($id);
    public function remove($id);
    public function headingReport($request);
}
