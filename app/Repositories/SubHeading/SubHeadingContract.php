<?php
namespace App\Repositories\SubHeading;

interface SubHeadingContract {
    public function create($request);
    public function edit($id, $request);
    public function findAll();
    public function findById($id);
    public function findByHeading($id);
    public function findPrice($id);
    public function allFixedSubheadings();
    public function remove($id);
    public function subHeadingReport($request);
}
