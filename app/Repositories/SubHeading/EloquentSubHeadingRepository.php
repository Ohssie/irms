<?php
namespace App\Repositories\SubHeading;

use App\Repositories\SubHeading\SubHeadingContract;
use App\SubHeading;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class EloquentSubHeadingRepository implements SubHeadingContract {

    public function create($request) {
        $sub = new SubHeading();
        $this->subProperties($sub, $request);
        $sub->save();
        return $sub;
    }

    public function  edit($id, $request) {
        $sub = $this->findById($id);
        $this->subProperties($sub, $request);
        $sub->save();
        return $sub;
    }

    public function findAll() {
        return SubHeading::all();
    }

    public function findById($id) {
        return SubHeading::find($id);
    }

    public function findByHeading($id) {
        if(Sentinel::getUser()->inRole('tax-payer')) {
            $subHeading = SubHeading::where('heading_id', $id)->where('pricing', 'fixed')->get();
        } else {
            $subHeading = SubHeading::where('heading_id', $id)->get();
        }
        return $subHeading;
    }

    public function allFixedSubheadings() {
        return SubHeading::where('pricing', 'fixed')->get();
    }

    public function findPrice($id) {
        $sub = $this->findById($id);
        return $sub;
    }

    public function remove($id) {
        $sub = $this->findById($id);
        return $sub->delete();
    }

    private function subProperties($sub, $request) {
        $sub->name = $request->name;
        $sub->code = $request->code;
        $sub->pricing = $request->pricing;
        $sub->amount = $request->amount;
        $sub->heading_id = $request->heading_id;
        $sub->recurrent = $request->recurrent;
        $sub->validity = $request->validity;
    }

    public function subHeadingReport($request)
    {
      if($request->month){
        $month_vars = explode("|", $request->month);
        $num_padded = sprintf("%02d", $month_vars[2]);
        $sub_headings = SubHeading::whereBetween('created_at', [$request->year.'-'.$num_padded.'-01', $request->year.'-'.$num_padded.'-'.$month_vars[1]])
                              ->orderBy('created_at', 'desc')->get();

      }elseif($request->day){
        $sub_headings = SubHeading::whereDate('created_at', $request->day)->orderBy('created_at', 'desc')->get();
      }
      return $sub_headings;
    }
}
