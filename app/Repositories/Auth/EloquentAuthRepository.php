<?php

namespace App\Repositories\Auth;

use App\Repositories\Auth\AuthContract;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Activation;
use App\User;
use Storage;
use Log;

class EloquentAuthRepository implements AuthContract
{
    public function createDataEntryPersonel($request){
      $avatarDetails = saveUserAvatar($request);

      $userDetails = [
            'full_name' => ucwords($request->name),
            'email' => $request->email,
            'password' => 'secret',
            'phone_number' => $request->phone_number,
            'avatar' => '/'.$avatarDetails['destination'].'/'.$avatarDetails['file_name'],
            'plateau_lga' => $request->lga,
          ];

        // $user = Sentinel::registerAndActivate($userDetails);
        $user = Sentinel::register($userDetails, false);

        if(isset($request->user_role)){
          $role = Sentinel::findRoleBySlug($request->user_role);
          $role->users()->attach($user);
        }

        $activation = Activation::create($user);

        return $user;
    }

    public function createTaxpayer($request)
    {
      $tax_payer_details = [
          'full_name' => ucwords($request->full_name),
          'email' => $request->email,
          'password' => $request->password,
          'phone_number' => $request->phone_number,
          'lga' => $request->lga,
          'taxpayer_group_id' => $request->tax_payer_group_id,
          'occupation' => $request->occupation,
          'address' => $request->address,
          'confirmation_code' => randomString(8),
      ];

      $user = Sentinel::registerAndActivate($tax_payer_details, false);
      $role = Sentinel::findRoleBySlug('tax-payer');
      $role->users()->attach($user);
      $activation = Activation::create($user);

      return $user;
    }

    public function updateDataEntryPersonel($request, $user_id)
    {
      $user = Sentinel::findById($user_id);
      $userDetails = [
            'full_name' => ucwords($request->name),
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'plateau_lga' => $request->plateau_lga,
          ];

      $user = Sentinel::update($user, $userDetails);

      return $user;
    }

    public function deleteDataEntryPersonel($user_id)
    {
      $user = Sentinel::findById($user_id);
      $user->delete();

      return true;
    }

    public function findUser($user_id)
    {
      return User::find($user_id);
    }

    public function confirmUser($request)
    {
      $user = $this->findUser($request->user_id);
      if($request->confirmation_code == $user->confirmation_code){
        $user->is_tax_payer_confirmed = 1;
        $user->save();
        return true;
      }

      return false;
    }
}
