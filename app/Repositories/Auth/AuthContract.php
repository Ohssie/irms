<?php
namespace App\Repositories\Auth;

interface AuthContract
{
    public function createDataEntryPersonel($request);
    public function updateDataEntryPersonel($request, $user_id);
    public function deleteDataEntryPersonel($user_id);

    public function createTaxpayer($request);
    public function findUser($user_id);
    public function confirmUser($request);
}
