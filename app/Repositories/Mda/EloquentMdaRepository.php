<?php
namespace App\Repositories\Mda;

use App\Repositories\Mda\MdaContract;
use App\Mda;

class EloquentMdaRepository implements MdaContract
{

    public function create($request)
    {
        $mda = new Mda();
        $this->mdaProperties($mda, $request);
        $mda->save();
        return $mda;
    }

    public function  edit($id, $request)
    {
        $mda = $this->findById($id);
        $this->mdaProperties($mda, $request);
        $mda->save();
        return $mda;
    }

    public function findAll()
    {
        return Mda::all();
    }

    public function findById($id) {
        return Mda::find($id);
    }

    public function remove($id) {
        $mda = $this->findById($id);
        return $mda->delete();
    }

    private function mdaProperties($mda, $request)
    {
        $mda->name = $request->name;
        $mda->code = $request->code;
    }
}
