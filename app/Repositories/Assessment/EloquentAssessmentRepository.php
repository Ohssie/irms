<?php
namespace App\Repositories\Assessment;

use App\Assessment;
use Sentinel;
use Illuminate\Support\Facades\Crypt;

class EloquentAssessmentRepository implements AssessmentContract
{
	public function getUserAssessments($user_id)
	{
		$assessments = Assessment::where('taxpayer_id', $user_id)->get();
		return $assessments;
	}

	public function saveAssessment($request)
	{
		$less_expenses = [];
		$total_less_expense = 0;
		for ($i=0; $i < count($request->less_expenses_item) ; $i++) {
			if( is_null($request->less_expenses_item[$i]) ) continue;

			$amount = is_null($request->less_expenses_amount[$i]) ? 0 : $request->less_expenses_amount[$i];
			$less_expenses[$request->less_expenses_item[$i]] = $amount;
			$total_less_expense += $amount;
		}

		$less_expenses['total_less_expense'] = $total_less_expense;


		$total_incomes = 0;
		$employee_income = [];
		for ($i=0; $i < count($request->employee_income_item) ; $i++) {
			if( is_null($request->employee_income_item[$i]) ) continue;

			$amount = is_null($request->employee_income_amount[$i]) ? 0 : $request->employee_income_amount[$i];

			$employee_income[$request->employee_income_item[$i]] = $amount;
			$total_incomes += $amount;
		}

		$other_income = [];
		for ($i=0; $i < count($request->other_income_item) ; $i++) {
			if( is_null($request->other_income_item[$i]) ) continue;

			$amount = is_null($request->other_income_amount[$i]) ? 0 : $request->other_income_amount[$i];

			$other_income[$request->other_income_item[$i]] = $amount;
			$total_incomes += $amount;

		}

		$employee_income['total_incomes'] = $total_incomes;
		$other_income['total_incomes'] = $total_incomes;

		$total_reliefs = 0;
		$relief_and_non_taxable_income = [];
		$relief_and_non_taxable_income["personal_allowance"] = $request->personal_allowance;
		$relief_and_non_taxable_income["national_housing_fund"] = $request->nhf;
		$relief_and_non_taxable_income["national_health_insurance"] = $request->nhi;
		$relief_and_non_taxable_income["life_assurance"] = $request->life_assurance;
		$relief_and_non_taxable_income["national_pension_scheme"] = $request->pension;
		$relief_and_non_taxable_income["dividend"] = $request->dividend;
		$relief_and_non_taxable_income["interest"] = $request->interest;

		$total_reliefs = $request->personal_allowance + $request->nhf + $request->nhi + $request->life_assurance + $request->pension + $request->dividend + $request->interest;
		$relief_and_non_taxable_income['total_reliefs'] = $total_reliefs;
		$tax_lists  = [ 7 => 300000, 11=>300000, 15=>500000, 19 => 500000, 21=> 1600000, 24=> 3200000, 24 => 1000000000];

		$taxable_amount = $total_reliefs;
		$annualTax     = 0;


		foreach ($tax_lists as $key => $tax) {

			if ($taxable_amount > $tax) {

					$former_worked = $taxable_amount;
					$taxed      = ($key / 100) * $tax;
					$taxable_amount = $taxable_amount - $tax;

					$annualTax += $taxed;
			} else {

					$former_worked = $taxable_amount;
					$taxed = ($key / 100) * $taxable_amount;

					$annualTax += $taxed;
					$taxCalculationComplete = true;
					break;
			}

		}

		if(!$taxCalculationComplete)
		{
				$annualTax += ( 24/100 ) * $taxable_amount;
		}

		$relief_and_non_taxable_income['tax'] = $annualTax;

		$assessment = Assessment::create([
			'assessment_year' => date('Y'),
			'postal_code' => null,
			'less_expenses' => json_encode($less_expenses),
			'employee_income' => json_encode($employee_income),
			'other_incomes' => json_encode($other_income),
			'relief_and_non_taxable_income' => json_encode($relief_and_non_taxable_income),
			'status' => 'PENDING',
			'taxpayer_id' => Sentinel::getUser()->id,
			'generated_by' => Sentinel::getUser()->id,
			'date_verified' => null,
		]);

		return $assessment;
	}

	public function findAssessment($assessment_id)
	{
		return Assessment::find($assessment_id);
	}
}
