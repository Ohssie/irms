<?php
namespace App\Repositories\Assessment;

interface AssessmentContract
{
	public function getUserAssessments($user_id);
	public function saveAssessment($request);
	public function findAssessment($assessment_id);
}
