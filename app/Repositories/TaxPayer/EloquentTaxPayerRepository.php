<?php
namespace App\Repositories\TaxPayer;

use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
class EloquentTaxPayerRepository implements TaxPayerContract
{
	public function create($request) {
		$taxPayer = new TaxPayer();
		$this->taxPayerProps($taxPayer, $request);
		$taxPayer->save();
		return $taxPayer;
	}

	public function edit($request, $id) {
		$taxPayer = $this->findById($id);
		$this->taxPayerProps($taxPayer, $request);
		$taxPayer->save();
		return $taxPayer;
	}

	public function findById($id) {
		return User::find($id);
	}

	public function findAll() {
		$role = Sentinel::findRoleBySlug('tax-payer');
		$taxpayers = $role->users()->with('roles')->get();
		return $taxpayers;
	}

	public function remove($id) {
		$taxPayer = $this->findById($id);
		return $taxPayer->delete();
	}

	private function taxPayerProps($taxPayer, $request) {
		if($request->avatar != null){
			$avatar = saveUserAvatar($request);
			$avatar = '/'.$avatar['destination'].'/'.$avatar['file_name'];
			$taxPayer->avatar = $avatar;
		}


		$unique_id = uniqueIdentifier();

		$taxPayer->unique_id = $unique_id;
		$taxPayer->full_name = $request->full_name;
		$taxPayer->phone_number = $request->phone_number;
		$taxPayer->address = $request->address;
		$taxPayer->tax_payer_group_id = $request->tax_payer_group_id;

		$taxPayer->email = $request->email;
		$taxPayer->lga = $request->lga;
		$taxPayer->occupation = $request->occupation;
	}

	public function taxPayerReport($request){
		if($request->month){
			$month_vars = explode("|", $request->month);
			$num_padded = sprintf("%02d", $month_vars[2]);
			$tax_payers = TaxPayer::whereBetween('created_at', [$request->year.'-'.$num_padded.'-01', $request->year.'-'.$num_padded.'-'.$month_vars[1]])
														->orderBy('created_at', 'desc')->get();

		}elseif($request->day){
			$tax_payers = TaxPayer::whereDate('created_at', $request->day)->orderBy('created_at', 'desc')->get();
		}
		return $tax_payers;
	}
}
