<?php
namespace App\Repositories\TaxPayer;

interface TaxPayerContract
{
	public function create($request);
	public function edit($request, $id);
	public function findById($id);
	public function findAll();
	public function remove($id);
	public function taxPayerReport($request);
}
