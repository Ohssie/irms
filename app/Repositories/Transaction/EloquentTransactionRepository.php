<?php
namespace App\Repositories\Transaction;

use App\Repositories\Transaction\TransactionContract;
use App\Transaction;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Repositories\TaxPayer\TaxPayerContract;

class EloquentTransactionRepository implements TransactionContract
{

  protected $taxpayer;

  public function __construct(TaxPayerContract $taxpayerContract) {
    $this->taxpayer = $taxpayerContract;
  }
  public function create($request)
  {
    $tranx = new Transaction;
    $this->transactionProps($tranx, $request);
    // dd($tranx);
    $tranx->save();
    return $tranx;
  }

  public function findAll()
  {
    $user = Sentinel::getUser();
    if(Sentinel::getUser()->inRole('tax-payer')) {
      $transactions = Transaction::where('user_id', $user->id)->get();
    } else {
      $transactions = Transaction::all();
    }
    return $transactions;
  }

  public function findById($id)
  {
    return Transaction::find($id);
  }

  public function todaysRemittance() {
    $tranx = $this->findAll();
    $total = 0;
    $record = 0;
    foreach($tranx as $tran) {
      if(date('d, M, y', strtotime($tran->created_at)) == date('d, M, y')) {
        $record++;
        $total = $total += $tran->amount;
      }
    }
    return $total;
  }

  public function thisMonthsRemittance() {
    $tranx = $this->findAll();
    $total = 0;
    $record = 0;
    foreach($tranx as $tran) {
      if(date('M, y', strtotime($tran->created_at)) == date('M, y')) {
        $record++;
        $total = $total += $tran->amount;
      }
    }
    return $total;
  }

  public function lastMonthsRemittance() {
    $tranx = $this->findAll();
    $total = 0;
    $record = 0;
    foreach($tranx as $tran) {
      if(date('F, y', strtotime($tran->created_at)) == date('F, y', strtotime("-1 Months"))) {
        $record++;
        $total = $total += $tran->amount;
      }
    }
    return $total;
  }

  public function totalFromInception() {
    $tranx = $this->findAll();
    $total = 0;
    foreach($tranx as $tran) {
      $total = $total += $tran->amount;
    }
    return $total;
  }

  private function transactionProps($tranx, $request) {
    $user = Sentinel::getUser();

    $tranx->tranx_id = $request['data']['id'];
    $tranx->tranx_ref = $request['data']['reference'];
    $tranx->gateway_response = $request['data']['gateway_response'];
    $tranx->authorization_code = $request['data']['authorization']['authorization_code'];
    $tranx->amount = $request['data']['amount']; //specified in kobo
    $tranx->taxpayer = $request['data']['metadata']['taxpayer'];
    $tranx->user_id = $user->id;
    // $tranx->assessment_id = $user->id;
  }
}
