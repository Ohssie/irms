<?php
namespace App\Repositories\Transaction;

interface TransactionContract
{
  public function create($request);
  public function findAll();
  public function findById($id);
  public function todaysRemittance();
  public function thisMonthsRemittance();
  public function lastMonthsRemittance();
  public function totalFromInception();
}
