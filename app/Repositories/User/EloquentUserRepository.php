<?php

namespace App\Repositories\User;

use App\Repositories\User\UserContract;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Activation;
use App\User;
use Storage;
use Log;
use Cartalyst\Sentinel\Roles\EloquentRole;

class EloquentUserRepository implements UserContract
{
    public function createUser($request){
      $avatarDetails = saveUserAvatar($request);

      $userDetails = [
        'full_name' => ucwords($request->name),
        'email' => $request->email,
        'password' => 'secret',
        'phone_number' => $request->phone_number,
        'avatar' => '/'.$avatarDetails['destination'].'/'.$avatarDetails['file_name'],
        'lga' => $request->lga,
      ];

      $user = Sentinel::register($userDetails, false);

      $user->mdas()->sync($request->mdas);

      $role = Sentinel::findRoleBySlug($request->role);
      $role->users()->attach($user);

      $activation = Activation::create($user);

      return $user;
    }

    public function updateUser($request, $user_id)
    {
      $user = Sentinel::findById($user_id);
      $userDetails = [
        'full_name' => ucwords($request->name),
        'email' => $request->email,
        'phone_number' => $request->phone_number,
        // 'avatar' => '/'.$avatarDetails['destination'].'/'.$avatarDetails['file_name'],
        'lga' => $request->lga,
      ];

      $updated_user = Sentinel::update($user, $userDetails);
      $updated_user->mdas()->sync($request->mdas);
      $role = Sentinel::findRoleBySlug($request->role);

      // $role->users()->sync($updated_user);
      return $user;
    }

    public function deleteUser($user_id) {
      $user = Sentinel::findById($user_id);
      $user->delete();

      return true;
    }

    public function findById($id) {
      $user = Sentinel::findById($id);
      return $user;
    }

    public function findAll() {
      $users = collect();
      $roles = EloquentRole::where('slug', '!=', 'tax-payer')->get();
      foreach($roles as $role) {
        $allUsers = $role->users()->get();
        $users->push($allUsers);
      }
      return $users->flatten();
    }

    public function findAllRoles() {
      return EloquentRole::all();
    }

    public function allTaxpayers() {
      $role = Sentinel::findRoleBySlug('tax-payer');
      $taxpayers = $role->users()->with('roles')->get();
      return $taxpayers;
    }

    public function findAllUsers() {
      return User::all();
    }
}
