<?php
namespace App\Repositories\User;

interface UserContract
{
    public function createUser($request);
    public function updateUser($request, $user_id);
    public function deleteUser($user_id);
    public function findById($user_id);
    public function findAll();
    public function findAllUsers();
    public function allTaxpayers();
}
