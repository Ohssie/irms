<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    protected $fillable = ['assessment_year','postal_code', 'less_expenses', 'employee_income', 'other_incomes', 'relief_and_non_taxable_income', 'status', 'taxpayer_id', 'generated_by', 'date_verified'];
    public function taxpayer()
    {
        return $this->belongsTo('App\User', 'taxpayer_id');
    }

    public function generateBy()
    {
        return $this->belongsTo('App\User', 'generated_by');
    }
}
