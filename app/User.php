<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \Cartalyst\Sentinel\Users\EloquentUser
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'full_name', 'avatar', 'phone_number', 'address', 'lga',
        'tax_payer_group_id', 'lga', 'confirmation_code', 'is_tax_payer_confirmed'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function headings()
    {
      return $this->hasMany('App\Headings');
    }

    public function mdas()
    {
      return $this->belongsToMany('App\Mda')->withTimestamps();
    }

    public function assessments() {
      return $this->hasMany('App\Assessment');
    }
}
