<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mda extends Model
{
  public function heading() {
      return $this->hasMany('App\Headng');
  }

  public function users()
  {
    return $this->belongsToMany('App\User')->withTimestamps();
  }
}
